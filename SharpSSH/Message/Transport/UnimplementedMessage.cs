﻿using SharpSSH.Util;
using System;
using System.Threading.Tasks;

namespace SharpSSH.Message
{
	public static class UnimplementedMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-11.4

		public static Task SendUnimplementedMessage(this IRemotePeer client, uint packetSequenceNumber)
		{
			using (var buffer = ByteArrayPool.Acquire(5))
			{
				var messageLength = Write(buffer, 0, packetSequenceNumber);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out uint packetSequenceNumber)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.Unimplemented)
				throw new FormatException($"Expecting message type: ${MessageType.Unimplemented.GetName()}");

			if (length != 5)
				throw new FormatException("Invalid message length");

			packetSequenceNumber = reader.ReadUInt32();
		}

		public static int Write(byte[] buffer, int offset, uint packetSequenceNumber)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.Unimplemented);
			writer.WriteUInt32(packetSequenceNumber);

			return writer.MessageLength;  // always 5
		}
	}
}
