﻿using System;

namespace SharpSSH.ServerHostKey
{
	public interface IServerHostKeyAlgorithm
	{
		int CreateKeyAndCertificatesData(byte[] keyAndCertificateData, int keyAndCertificateOffset);
		int CreateSignatureData(byte[] hash, byte[] signatureData, int signatureDataOffset);
	}
}
