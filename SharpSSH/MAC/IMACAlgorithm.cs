﻿using System;

namespace SharpSSH.MAC
{
	public interface IMACAlgorithm
	{
		int KeySize { get; }

		int DigestLength { get; }

		void SetKey(byte[] key, int offset, int length);

		void ComputeHash(uint packetNumber, byte[] data, int dataOffset, int dataCount, byte[] hash, int hashOffset);
	}
}
