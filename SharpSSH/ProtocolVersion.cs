﻿using System;
using System.Text.RegularExpressions;

using static SharpSSH.SSHConstants;

namespace SharpSSH
{
	public struct ProtocolVersion
	{
		// SSH-protoversion-softwareversion SP comments
		static readonly Regex regex = new Regex(@"^SSH-([\x21-\x2C\x2E-\x7E]+)-([\x21-\x2C\x2E-\x7E]+)(?: (.+))?$");

		/// <summary>Raw identification string, excluding <c>CR LF</c>.</summary>
		public readonly string IdentificationString;

		/// <summary>Check if the identification string is valid.</summary>
		public bool IsValid => IdentificationString.Length <= PROTOCOL_VERSION_STRING_MAX_CHARACTERS && regex.IsMatch(IdentificationString);

		/// <summary>Protocol version (example: <c>"2.0"</c>), <c>null</c> if invalid.</summary>
		public string ProtoVersion => IsValid ? regex.Match(IdentificationString).Groups[1].Value : null;

		/// <summary>Software version, <c>null</c> if invalid.</summary>
		public string SoftwareVersion => IsValid ? regex.Match(IdentificationString).Groups[2].Value : null;

		/// <summary>Comments, might be an empty string, <c>null</c> if invalid.</summary>
		public string Comments => IsValid ? regex.Match(IdentificationString).Groups[3].Value : null;

		/// <summary>Wrap an <paramref name="identificationString"/> in a <see cref="ProtocolVersion"/> struct.</summary>
		/// <param name="identificationString">Raw identification string, excluding <c>CR LF</c>.</param>
		public ProtocolVersion(string identificationString) => IdentificationString = identificationString ?? "";

		public ProtocolVersion(string protoVersion, string softwareVersion, string comments) : this($"SSH-{protoVersion}-{softwareVersion}" + (String.IsNullOrEmpty(comments) ? "" : $" {comments}")) { }

		public override string ToString() => IdentificationString;
	}
}
