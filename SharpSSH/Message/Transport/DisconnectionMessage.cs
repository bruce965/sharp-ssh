﻿using SharpSSH.Util;
using System;
using System.Text;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class DisconnectionMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-11.1

		public static Task SendDisconnectionMessage(this IRemotePeer client, DisconnectionReason reason, string description, string language)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0, reason, description, language);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out DisconnectionReason reason, out string description, out string language)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.Disconnect)
				throw new FormatException($"Expecting message type: {MessageType.Disconnect.GetName()}");

			reason = (DisconnectionReason)reader.ReadUInt32();
			description = reader.ReadString(Encoding.UTF8);
			language = reader.ReadString(Encoding.ASCII);

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset, DisconnectionReason reason, string description, string language)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.Disconnect);
			writer.WriteUInt32((uint)reason);
			writer.WriteString(description, Encoding.UTF8);
			writer.WriteString(language, Encoding.ASCII);

			return writer.MessageLength;
		}
	}
}
