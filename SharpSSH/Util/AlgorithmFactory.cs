﻿using System;

namespace SharpSSH.Util
{
	public class AlgorithmFactory
	{
		class Factory1<T> : IAlgorithmFactory<T> where T : new()
		{
			public string AlgorithmName { get; private set; }

			public T Instantiate() => new T();

			public Factory1(string algorithmName)
			{
				AlgorithmName = algorithmName;
			}
		}

		class Factory2<T> : IAlgorithmFactory<T>
		{
			public string AlgorithmName { get; private set; }

			readonly T singleton;

			public T Instantiate() => singleton;

			public Factory2(string algorithmName, T singleton)
			{
				AlgorithmName = algorithmName;
				this.singleton = singleton;
			}
		}

		public static IAlgorithmFactory<T> For<T>(string algorithmName) where T : new()
		{
			return new Factory1<T>(algorithmName);
		}

		public static IAlgorithmFactory<T> For<T>(string algorithmName, T singleton)
		{
			return new Factory2<T>(algorithmName, singleton);
		}
	}
}
