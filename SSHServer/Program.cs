﻿using SharpSSH.ServerHostKey;
using SharpSSH.Util;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace SharpSSH.Server
{
	static class Program
	{
		static void Main(string[] args)
		{
			// start an SSH server and handle all possible events
			var server = new SSHServer(IPAddress.Any, 22);
			server.Exception += (sender, e) => Console.Error.WriteLine($"{e.Exception.GetType().Name} from {e.RemotePeer?.ToString() ?? "server"}{Environment.NewLine}{e.Exception}");
			server.Starting += (sender, e) => Console.WriteLine("Starting SSH server...");
			server.Started += (sender, e) => Console.WriteLine("Started!");
			server.Stopping += (sender, e) => Console.WriteLine("Stopping SSH server...");
			server.Stopped += (sender, e) => Console.WriteLine("Stopped!");
			server.Connected += (sender, e) => Console.WriteLine($"Connected: {e.RemotePeer}");
			server.SoftwareVersionReceived += (sender, e) => Console.WriteLine($"Protocol version received from {e.RemotePeer}");
			server.Disconnected += (sender, e) => Console.WriteLine($"Disconnected: {e.RemotePeer}");
			server.SendingMessage += (sender, e) => Console.WriteLine($"Sending {((MessageType)e.Payload.ReadByte()).GetName()} to {e.RemotePeer}");
			server.MessageReceived += (sender, e) => Console.WriteLine($"Received {((MessageType)e.Payload.ReadByte()).GetName()} from {e.RemotePeer}");
			server.DebugMessageReceived += (sender, e) => Console.WriteLine($"Debug message from {e.RemotePeer}: {e.Message}");

			// import host key
			using (var stream = Assembly.GetAssembly(typeof(Program)).GetManifestResourceStream($"{typeof(Program).Namespace}.test_key.xml"))
			using (var buffer = ByteArrayPool.Acquire((int)stream.Length))
			using (var memory = new MemoryStream(buffer.Array, true))
			{
				// read to memory
				stream.CopyTo(memory);

				// create a new hostkey factory with the specified key
				server.SupportedServerHostKeyAlgorithms.Add(SSHRSAServerHostKeyAlgorithm.CreateFactory(buffer.Array));

				// clear buffer before returning to the pool because it contains sensitive data
				Array.Clear(buffer.Array, 0, buffer.Array.Length);
			}

			// stop SSH server when the process receives a SIGINT
			Console.CancelKeyPress += (sender, e) => server.Stop();

			// create a Task that will be completed after SSH server is stopped
			var stopped = new TaskCompletionSource<bool>();
			server.Stopped += (sender, e) => stopped.SetResult(true);

			// start SSH server
			server.Start();

			Console.WriteLine("Press CTRL + C to stop SSH server...");

			// wait for the SSH server to be stopped
			stopped.Task.Wait();
		}
	}
}
