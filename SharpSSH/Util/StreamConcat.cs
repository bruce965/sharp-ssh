﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SharpSSH.Util
{
	public class StreamReadConcat : Stream
	{
		readonly Queue<Stream> streams;

		public StreamReadConcat(IEnumerable<Stream> streams)
		{
			this.streams = new Queue<Stream>(streams);
		}

		public override bool CanRead => true;

		public override bool CanSeek => false;

		public override bool CanWrite => false;

		public override long Length => streams.Sum(stream => stream.Length);

		public override long Position
		{
			get => throw new NotSupportedException();
			set => throw new NotSupportedException();
		}

		public override void Flush()
		{
			throw new NotSupportedException();
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int totalRead = 0;
			while (totalRead < count)
			{
				if (streams.Count == 0)
					return totalRead;

				var read = streams.Peek().Read(buffer, offset + totalRead, count - totalRead);
				if (read == 0)
					streams.Dequeue();

				totalRead += read;
			}

			return totalRead;
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}
	}
}
