﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSSH
{
	public partial class SSHServer : IDisposable
	{
		#region Events

		/// <summary>SSH client/server is starting.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Starting;

		/// <summary>SSH client/server is started.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Started;

		/// <summary>SSH client/server is stopping.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Stopping;

		/// <summary>SSH client/server is stopped and all remote peers disconnected.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Stopped;

		/// <summary>Raise the <see cref="Starting"/> event to report the SSH client/server is starting.</summary>
		protected virtual void RaiseStarting()
		{
			if (Starting == null)
				return;

			raiseSafely(Starting, SSHServerEventArgs.Empty);
		}

		/// <summary>Raise the <see cref="Started"/> event to report the SSH client/server is started.</summary>
		protected virtual void RaiseStarted()
		{
			if (Started == null)
				return;

			raiseSafely(Started, SSHServerEventArgs.Empty);
		}

		/// <summary>Raise the <see cref="Stopping"/> event to report the SSH client/server is stopping.</summary>
		protected virtual void RaiseStopping()
		{
			if (Stopping == null)
				return;

			raiseSafely(Stopping, SSHServerEventArgs.Empty);
		}

		/// <summary>Raise the <see cref="Stopped"/> event to report the SSH client/server is stopped and all remote peer disconnected.</summary>
		protected virtual void RaiseStopped()
		{
			if (Stopped == null)
				return;

			raiseSafely(Stopped, SSHServerEventArgs.Empty);
		}

		#endregion

		// TODO: this property is here for the day I will make this class work both as a server and as a client
		readonly bool isServer = true;

		readonly IPAddress localAddress;
		readonly int port;

		readonly object tcpServerCTSLock = new object();
		CancellationTokenSource tcpServerCTS;

		public SSHServer(IPAddress localAddress, int port)
		{
			this.localAddress = localAddress;
			this.port = port;
		}

		/// <summary>Starts listening for incoming connection requests.</summary>
		public void Start() => Start(Int32.MaxValue);

		/// <summary>Starts listening for incoming connection requests with a maximum number of pending connection.</summary>
		/// <param name="backlog">The maximum length of the pending connections queue.</param>
		public void Start(int backlog)
		{
			lock (tcpServerCTSLock)
			{
				// if already running, do nothing
				if (tcpServerCTS != null)
					return;

				// raise "starting" event
				RaiseStarting();

				// create a TCP server
				var tcpServer = new TcpListener(localAddress, port);

				// create a cancellation token, which terminates server when cancelled
				tcpServerCTS = new CancellationTokenSource();
				tcpServerCTS.Token.Register(() =>
				{
					try
					{
						tcpServer.Stop();
					}
					catch (Exception)
					{
						// should never happen unless server was already dead
					}
				});

				// start the TCP server
				try
				{
					tcpServer.Start(backlog);
				}
				catch (Exception)
				{
					lock (tcpServerCTSLock)
						stop(ref tcpServerCTS);

					throw;
				}

				// start accepting connections
				var serverTask = startAcceptingConnections(tcpServer, tcpServerCTS.Token).ContinueWith(task =>
				{
					if (task.IsFaulted)
						RaiseException(null, task.Exception);

					// if server connection accepter dies unexpectedly, kill the whole server
					lock (tcpServerCTSLock)
						stop(ref tcpServerCTS);
				});

				handleExceptions(null, serverTask);

				// raise "started" event
				RaiseStarted();
			}
		}

		/// <summary>Stops listening for incoming connections and terminates all active connections.</summary>
		public void Stop()
		{
			lock (tcpServerCTSLock)
				stop(ref tcpServerCTS);
		}

		void stop(ref CancellationTokenSource cts)
		{
			if (cts != null)
			{
				// raise "stopping" event
				RaiseStopping();

				// cancel the server token, terminating the server synchronously
				cts.Cancel();  // will never throw
				cts = null;

				// disconnect all clients
				lock (connectedClients)
				{
					foreach (var client in connectedClients)
					{
						try
						{
							client.Disconnect(DisconnectionReason.ByApplication, "Closing", null);
						}
						catch (Exception e)
						{
							RaiseException(client, e);
						}
					}
				}

				// TODO: raise "stopped" event only after all clients have been disconnected

				// raise "stopped" event
				RaiseStopped();
			}
		}

		/// <summary>Start accepting connections asynchronously.</summary>
		/// <param name="tcpServer">The server to acept connections from.</param>
		/// <param name="ct">Cancellation token to stop accepting connections and disconnect all clients.</param>
		/// <returns>A task completed when the server is stopped.</returns>
		async Task startAcceptingConnections(TcpListener tcpServer, CancellationToken ct)
		{
			while (!ct.IsCancellationRequested)
			{
				TcpClient tcpClient;
				try
				{
					tcpClient = await tcpServer.AcceptTcpClientAsync();
				}
				catch (ObjectDisposedException)
				{
					// already closed
					return;
				}

				var clientCTS = CancellationTokenSource.CreateLinkedTokenSource(ct);

				RemotePeer client = null;
				client = new RemotePeer(!this.isServer, tcpClient, (msg, offset, length) => sendPacket(client, msg, offset, length, clientCTS.Token), clientCTS);

				lock (connectedClients)
					connectedClients.Add(client);

				var clientTask = startReceivingFromRemotePeer(client, clientCTS.Token).ContinueWith(task =>
				{
					lock (connectedClients)
						connectedClients.Remove(client);

					clientCTS.Cancel();

					if (task.IsFaulted)
						RaiseException(client, task.Exception);

					disconnectRemotePeer(client);
				});

				handleExceptions(client, clientTask);

				// raise "connected" event
				RaiseConnected(client);
			}
		}

		void IDisposable.Dispose()
		{
			Stop();
		}
	}
}
