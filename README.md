Sharp SSH
=========

*NOTE: this project is still a work-in-progress!*

SharpSSH is a fully managed SSH 2.0 implementation in C# .NET.

My idea with SharpSSH is to make an SSH client/server with low memory and
garbage footprint (about 150KB per-connected client), no polling, and zero
hanging threads while waiting for messages to be processed.

I followed [this wonderful guide](https://github.com/TyrenDe/SSHServer/wiki)
while writing SharpSSH.
If you are interested in learning how the SSH protocol and making your own
implementation, I suggest reading this guide.

.NET Core support is planned, I will look into it after completing the
standard implementation.

## License

SharpSSH is released under the terms of [The MIT License](LICENSE).
