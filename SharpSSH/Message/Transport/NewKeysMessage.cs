﻿using SharpSSH.Util;
using System;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class NewKeysMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-7.3

		public static Task SendNewKeysMessage(this IRemotePeer client)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.NewKeys)
				throw new FormatException($"Expecting message type: {MessageType.NewKeys.GetName()}");

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.NewKeys);

			return writer.MessageLength;
		}
	}
}
