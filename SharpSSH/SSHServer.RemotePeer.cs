﻿using SharpSSH.Message;
using SharpSSH.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH
{
	partial class SSHServer
	{
		/// <summary>Contains data about a peer connected to this SSH client/server.</summary>
		protected partial class RemotePeer : IRemotePeer
		{
			public bool IsServer { get; private set; }

			/// <summary>TCP client for this remote peer.</summary>
			public readonly TcpClient TcpClient;

			/// <summary>TCP stream to this remote peer.</summary>
			public readonly NetworkStream Stream;

			/// <summary>Cancel this to disconnect this peer.</summary>
			readonly CancellationTokenSource cts;

			public EndPoint RemoteEndPoint { get; private set; }

			public RemotePeer(bool isServer, TcpClient tcpClient, SendMessageDelegate sendMessage, CancellationTokenSource cts)
			{
				IsServer = isServer;
				TcpClient = tcpClient;
				Stream = tcpClient.GetStream();

				this.cts = cts;
				this.sendMessage = sendMessage;

				RemoteEndPoint = tcpClient.Client.RemoteEndPoint;
			}

			#region Disconnect

			readonly object disconnectLock = new object();
			DisconnectionReason disconnectReason = DisconnectionReason.None;
			string disconnectDescription = null;
			string disconnectDescriptionLanguage = null;

			public DisconnectionReason DisconnectReason
			{
				get { lock (disconnectLock) return disconnectReason; }
				private set { lock (disconnectLock) disconnectReason = value; }
			}

			public string DisconnectDescription
			{
				get { lock (disconnectLock) return disconnectDescription; }
				private set { lock (disconnectLock) disconnectDescription = value; }
			}

			public string DisconnectDescriptionLanguage
			{
				get { lock (disconnectLock) return disconnectDescriptionLanguage; }
				private set { lock (disconnectLock) disconnectDescriptionLanguage = value; }
			}

			/// <summary>Disconnect without sending a disconnection message to the client.</summary>
			public void DisconnectImmediate() => Disconnect(DisconnectionReason.None);

			public void Disconnect(DisconnectionReason disconnectReason, string description, string language)
			{
				lock (disconnectLock)
				{
					// if already disconnected, ignore
					if (cts.IsCancellationRequested)
						return;

					DisconnectReason = disconnectReason;
					DisconnectDescription = description ?? "";
					DisconnectDescriptionLanguage = language ?? "";

					cts.Cancel();
				}
			}

			public void Disconnect(DisconnectionReason disconnectReason) => Disconnect(disconnectReason, disconnectReason.GetName(), null);

			#endregion

			#region Random

			readonly Random random = new Random();

			public void FillRandomBytes(byte[] data, int offset, int count)
			{
				lock (random)
				{
					for (var i = 0; i < count; i++)
					{
						data[offset + i] = unchecked((byte)random.Next());
					}
				}
			}

			#endregion

			public override string ToString() => $"{RemoteEndPoint} {ProtocolVersion}";
		}

		#region Events

		/// <summary>Connection with a remote peer estabilished.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Connected;

		/// <summary>Connection with a remote peer closed.</summary>
		public event SSHServerEvent<SSHServerEventArgs> Disconnected;

		/// <summary>A remote peer sent the software version string.</summary>
		public event SSHServerEvent<SSHServerEventArgs> SoftwareVersionReceived;

		/// <summary>Raise the <see cref="Connected"/> event to report a connection with a remote peer has been estabilished.</summary>
		protected virtual void RaiseConnected(IRemotePeer remotePeer)
		{
			if (Connected == null)
				return;

			raiseSafely(Connected, new SSHServerEventArgs(remotePeer));
		}

		/// <summary>Raise the <see cref="Disconnected"/> event to report a connection with a remote peer has been estabilished.</summary>
		protected virtual void RaiseDisconnected(IRemotePeer remotePeer)
		{
			if (Disconnected == null)
				return;

			raiseSafely(Disconnected, new SSHServerEventArgs(remotePeer));
		}

		/// <summary>Raise the <see cref="SoftwareVersionReceived"/> event to report a remote peer sent the software version string.</summary>
		protected virtual void RaiseSoftwareVersionReceived(IRemotePeer remotePeer)
		{
			if (SoftwareVersionReceived == null)
				return;

			raiseSafely(SoftwareVersionReceived, new SSHServerEventArgs(remotePeer));
		}

		#endregion

		readonly List<RemotePeer> connectedClients = new List<RemotePeer>();

		/// <summary>The connected clients.</summary>
		public IEnumerable<IRemotePeer> ConnectedClients
		{
			get
			{
				lock (connectedClients)
					return connectedClients.ToArray();
			}
		}

		/// <summary>Send the disconnection message and close the TCP connection.</summary>
		/// <param name="remotePeer">Remote peer to be disconnected.</param>
		void disconnectRemotePeer(RemotePeer remotePeer)
		{
			try
			{
				if (remotePeer.DisconnectReason == DisconnectionReason.None)
				{
					// close immediately
					remotePeer.TcpClient.Close();
				}
				else
				{
					using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
					{
						var cts = new CancellationTokenSource(TimeSpan.FromSeconds(30));
						var messageLength = DisconnectionMessage.Write(buffer, 0, remotePeer.DisconnectReason, remotePeer.DisconnectDescription, remotePeer.DisconnectDescriptionLanguage);

						// send a disconnection message, and close TCP client as soon as it is sent
						// if message is not sent before a timeout, cancel operation and disconnect client anyway
						remotePeer.SendMessage(buffer, 0, messageLength, cts.Token).ContinueWith(task => remotePeer.TcpClient.Close());
					}
				}
			}
			catch (Exception e)
			{
				RaiseException(remotePeer, e);

				try
				{
					// let's not forget to disconnect client in case of error
					remotePeer.TcpClient.Close();
				}
				catch (Exception e2)
				{
					RaiseException(remotePeer, e2);
				}
			}

			// raise "disconnected" event
			RaiseDisconnected(remotePeer);
		}

		/// <summary>
		/// Read messages from <paramref name="remotePeer"/> and keep-alive.
		/// Each client owns a <see cref="Task"/> returned by this method.
		/// </summary>
		/// <param name="remotePeer">The remote peer to start receiving packets from.</param>
		/// <param name="cancellationToken">Cancellation token to stop receiving packets.</param>
		/// <returns>Task completed after <paramref name="cancellationToken"/> is cancelled.</returns>
		async Task startReceivingFromRemotePeer(RemotePeer remotePeer, CancellationToken cancellationToken)
		{
			#region Protocol Version Exchange

			var protocolVersionOutputTask = sendProtocolVersionExchange(remotePeer.Stream, cancellationToken);
			var protocolVersionInputTask = readProtocolVersionExchange(remotePeer, cancellationToken);
			await Task.WhenAll(protocolVersionOutputTask, protocolVersionInputTask);

			var protocolVersionExchange = protocolVersionInputTask.Result;
			if (!protocolVersionExchange.HasValue)
			{
				// client has already been disconnected if we don't get a protocol version
				return;
			}

			remotePeer.ProtocolVersion = protocolVersionExchange.Value;

			try
			{
				// raise "software version received" event
				RaiseSoftwareVersionReceived(remotePeer);

				if (cancellationToken.IsCancellationRequested)
				{
					// disconnected by implementor
					// we save some CPU power by short-circuiting here returning immediately if disconnected
					return;
				}
			}
			catch (Exception e)
			{
				RaiseException(remotePeer, e);
			}

			if (remotePeer.ProtocolVersion.ProtoVersion != PROTO_VERSION)
			{
				remotePeer.Disconnect(DisconnectionReason.ProtocolVersionNotSupported, $"Protocol version not supported, only {PROTO_VERSION} is supported", null);
				return;
			}

			#endregion

			#region Key Exchange Initialization

			using (var cookieBuffer = ByteArrayPool.Acquire(16))
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				remotePeer.FillRandomBytes(cookieBuffer.Array, 0, 16);

				var messageLength = KeyExchangeInitializationMessage.Write(
					buffer, 0,
					cookieBuffer.Array,
					SupportedKeyExchangeAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedServerHostKeyAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedEncryptionAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedEncryptionAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedMacAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedMacAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedCompressionAlgorithms.Select(alg => alg.AlgorithmName),
					SupportedCompressionAlgorithms.Select(alg => alg.AlgorithmName),
					isServer ? PreferredLanguages : AvailableLanguages,
					isServer ? AvailableLanguages : PreferredLanguages,
					false
				);

				await remotePeer.SendMessage(buffer, 0, messageLength);

				var kexInitMessage = buffer.Array.Slice(0, messageLength);

				// NOTE: `I_S` must always be used with a `lock` over `ExchangeContext`.
				lock (remotePeer.ExchangeContext)
				{
					if (this.isServer)
						remotePeer.I_S = kexInitMessage;
					else
						remotePeer.I_C = kexInitMessage;
				}
			}

			#endregion
			
			while (!cancellationToken.IsCancellationRequested)
			{
				#region Read Incoming Packets

				try
				{
					await readPacket(remotePeer, cancellationToken);
				}
				catch (Exception e)
				{
					RaiseException(remotePeer, e);

					remotePeer.Disconnect(DisconnectionReason.ByApplication, "Error while reading packet", null);
					return;
				}

				#endregion
			}
		}
	}
}
