﻿using SharpSSH.Util;

namespace SharpSSH.MAC
{
	public class NoMACAlgorithm : IMACAlgorithm
	{
		public static readonly string AlgorithmName = "none";

		public static readonly NoMACAlgorithm Instance = new NoMACAlgorithm();

		public static readonly IAlgorithmFactory<NoMACAlgorithm> Factory = AlgorithmFactory.For(AlgorithmName, Instance);

		public int KeySize => 0;

		public int DigestLength => 0;

		public void SetKey(byte[] key, int offset, int length) { }

		public void ComputeHash(uint packetNumber, byte[] data, int dataOffset, int dataCount, byte[] hash, int hashOffset) { }
	}
}
