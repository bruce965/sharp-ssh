﻿using SharpSSH.Util;
using System;
using System.Text;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class DebugMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-11.1

		public static Task SendDebugMessage(this IRemotePeer client, bool alwaysDisplay, string message, string language)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0, alwaysDisplay, message, language);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out bool alwaysDisplay, out string message, out string language)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.Debug)
				throw new FormatException($"Expecting message type: {MessageType.Debug.GetName()}");

			alwaysDisplay = reader.ReadBoolean();
			message = reader.ReadString(Encoding.UTF8);
			language = reader.ReadString(Encoding.ASCII);

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset, bool alwaysDisplay, string message, string language)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.Debug);
			writer.WriteBoolean(alwaysDisplay);
			writer.WriteString(message, Encoding.UTF8);
			writer.WriteString(language, Encoding.ASCII);

			return writer.MessageLength;
		}
	}
}
