﻿using System;

namespace SharpSSH.Util
{
	public static class BitConverterBigEndian
	{
		public static void FromInt32(int value, byte[] bytes, int offset)
		{
			bytes[offset] = unchecked((byte)(value >> 24));
			bytes[offset + 1] = unchecked((byte)(value >> 16));
			bytes[offset + 2] = unchecked((byte)(value >> 8));
			bytes[offset + 3] = unchecked((byte)value);
		}

		public static int ToInt32(byte[] value, int offset) => (value[offset] << 24) | (value[offset + 1] << 16) | (value[offset + 2] << 8) | value[offset + 3];

		public static void FromUInt32(uint value, byte[] bytes, int offset) => FromInt32(unchecked((int)value), bytes, offset);

		public static uint ToUInt32(byte[] value, int offset) => unchecked((uint)ToInt32(value, offset));
	}
}
