﻿using SharpSSH.Util;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

namespace SharpSSH.ServerHostKey
{
	public class SSHRSAServerHostKeyAlgorithm : IServerHostKeyAlgorithm
	{
		public class AlgorithmFactory : IAlgorithmFactory<SSHRSAServerHostKeyAlgorithm>
		{
			public string AlgorithmName => SSHRSAServerHostKeyAlgorithm.AlgorithmName;

			public SSHRSAServerHostKeyAlgorithm Instantiate() => new SSHRSAServerHostKeyAlgorithm(key);

			readonly byte[] key;

			public AlgorithmFactory(byte[] key, int offset, int length)
			{
				this.key = key.Slice(offset, length);
			}
		}

		public static readonly string AlgorithmName = "ssh-rsa";

		public static AlgorithmFactory CreateFactory(byte[] key) => CreateFactory(key, 0, key.Length);

		public static AlgorithmFactory CreateFactory(byte[] key, int offset, int length) => new AlgorithmFactory(key, offset, length);

		readonly RSA rsa = RSA.Create();

		public SSHRSAServerHostKeyAlgorithm(byte[] key)
		{
			// TODO: parse as ASN.1 instead of XML

			var doc = new XmlDocument();
			doc.LoadXml(Encoding.ASCII.GetString(key));

			XmlElement root = doc["RSAKeyValue"];

			rsa.ImportParameters(new RSAParameters()
			{
				Modulus = Convert.FromBase64String(root["Modulus"].InnerText),
				Exponent = Convert.FromBase64String(root["Exponent"].InnerText),
				P = Convert.FromBase64String(root["P"].InnerText),
				Q = Convert.FromBase64String(root["Q"].InnerText),
				DP = Convert.FromBase64String(root["DP"].InnerText),
				DQ = Convert.FromBase64String(root["DQ"].InnerText),
				InverseQ = Convert.FromBase64String(root["InverseQ"].InnerText),
				D = Convert.FromBase64String(root["D"].InnerText)
			});
		}

		public int CreateKeyAndCertificatesData(byte[] keyAndCertificateData, int keyAndCertificateOffset)
		{
			// https://msdn.microsoft.com/en-us/library/ms867080.aspx
			// «.NET RSAParameters properties are big-endian ordered»
			var parameters = rsa.ExportParameters(false);

			var messageWriter = new SSHMessageWriter(keyAndCertificateData, keyAndCertificateOffset);
			messageWriter.WriteString(AlgorithmName, Encoding.ASCII);
			messageWriter.WriteMPInt(parameters.Exponent, false);
			messageWriter.WriteMPInt(parameters.Modulus, false);

			return messageWriter.MessageLength;
		}

		public int CreateSignatureData(byte[] hash, byte[] signatureData, int signatureDataOffset)
		{
			var messageWriter = new SSHMessageWriter(signatureData, signatureDataOffset);
			messageWriter.WriteString(AlgorithmName, Encoding.ASCII);
			messageWriter.WriteBytes(rsa.SignData(hash, HashAlgorithmName.SHA1, RSASignaturePadding.Pkcs1));

			return messageWriter.MessageLength;
		}
	}
}
