﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SharpSSH.Util
{
	public static class StreamExtensions
	{
		/// <summary>
		/// Read at least <paramref name="minCount"/> bytes from <paramref name="stream"/> at <paramref name="offset"/> position.
		/// Might read less bytes if <paramref name="stream"/> is shorter than <paramref name="minCount"/>.
		/// </summary>
		/// <param name="stream">Stream to read from.</param>
		/// <param name="buffer">Buffer used to store read bytes.</param>
		/// <param name="offset">Offset in <paramref name="buffer"/> to start writing at.</param>
		/// <param name="minCount">Read at least this many bytes before giving up (unless <paramref name="stream"/> is closed before).</param>
		/// <param name="maxCount">Read up to this many bytes.</param>
		/// <param name="ct">Cancellation token used to interrupt the operation.</param>
		/// <returns>The number of bytes read from the stream.</returns>
		public static async Task<int> ReadAtLeast(this Stream stream, byte[] buffer, int offset, int minCount, int maxCount, CancellationToken ct)
		{
			var totalRead = 0;

			while (totalRead < minCount)
			{
				try
				{
					var read = await stream.ReadAsync(buffer, offset + totalRead, maxCount - totalRead, ct);
					if (read == 0)
						break;

					totalRead += read;
				}
				catch (EndOfStreamException)
				{
					break;
				}
				catch (ObjectDisposedException)
				{
					break;
				}
			}

			return totalRead;
		}

		/// <summary>Concat two readable streams.</summary>
		/// <param name="stream">First stream.</param>
		/// <param name="other">Second stream (after first is consumed).</param>
		/// <returns>A stream containing the data from the first stream followed by the data in the second stream.</returns>
		public static Stream Concat(this Stream stream, Stream other) => new StreamReadConcat(new[] { stream, other });
	}
}
