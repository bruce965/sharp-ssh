﻿using System;

namespace SharpSSH.Compression
{
	public interface ICompressionAlgorithm
	{
		int Compress(byte[] data, int dataOffset, int dataCount, byte[] compressedData, int compressedDataOffset);

		int Decompress(byte[] data, int dataOffset, int dataCount, byte[] decompressedData, int decompressedDataOffset);
	}
}
