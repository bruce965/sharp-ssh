﻿using SharpSSH.Util;
using System;
using System.Numerics;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class DiffieHellmanKeyExchangeInitializationMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-8

		public static Task SendDiffieHellmanKeyExchangeInitializationMessage(this IRemotePeer client, BigInteger e)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0, e);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out BigInteger e)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.DiffieHellmanKeyExchangeInitialization)
				throw new FormatException($"Expecting message type: {MessageType.DiffieHellmanKeyExchangeInitialization.GetName()}");

			e = reader.ReadMPInt();

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset, BigInteger e)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.DiffieHellmanKeyExchangeInitialization);
			writer.WriteMPInt(e);

			return writer.MessageLength;
		}
	}
}
