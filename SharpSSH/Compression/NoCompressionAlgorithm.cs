﻿using SharpSSH.Util;

namespace SharpSSH.Compression
{
	public class NoCompressionAlgorithm : ICompressionAlgorithm
	{
		public static readonly string AlgorithmName = "none";

		public static readonly NoCompressionAlgorithm Instance = new NoCompressionAlgorithm();

		public static readonly IAlgorithmFactory<NoCompressionAlgorithm> Factory = AlgorithmFactory.For(AlgorithmName, Instance);

		public int Compress(byte[] data, int dataOffset, int dataCount, byte[] compressedData, int compressedDataOffset)
		{
			data.CopyTo(dataOffset, compressedData, compressedDataOffset, dataCount);
			return dataCount;
		}

		public int Decompress(byte[] data, int dataOffset, int dataCount, byte[] decompressedData, int decompressedDataOffset)
		{
			data.CopyTo(dataOffset, decompressedData, decompressedDataOffset, dataCount);
			return dataCount;
		}
	}
}
