﻿using System;

namespace SharpSSH.Util
{
	/// <summary>Pool to alleviate burden to the garbage collector.</summary>
	public static class ByteArrayPool
	{
		// TODO: implement byte[] pooling.

		public sealed class Reference : IDisposable
		{
			public byte[] Array { get; private set; }

			public Reference(byte[] array)
			{
				Array = array;
			}

			public static implicit operator byte[](Reference reference)
			{
				return reference.Array;
			}

			void IDisposable.Dispose()
			{
				Release(Array);
				Array = null;
			}
		}

		/// <summary>Acquire a reference to an array of bytes from the pool.</summary>
		/// <param name="minLength">The array of bytes will be at least of this length.</param>
		/// <returns>An array of bytes from the pool.</returns>
		public static Reference Acquire(int minLength)
		{
			return new Reference(new byte[minLength]);
		}

		/// <summary>Add an array of bytes to the pool.</summary>
		/// <param name="array">The array.</param>
		public static void Release(byte[] array)
		{
            
		}
	}
}
