﻿using SharpSSH.Util;
using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using static SharpSSH.SSHConstants;

namespace SharpSSH.KeyExchange
{
	public class DiffieHellmanGroup14SHA1KeyExchangeAlgorithm : IKeyExchangeAlgorithm
	{
		// https://tools.ietf.org/html/rfc4253#section-8

		public static readonly string AlgorithmName = "diffie-hellman-group14-sha1";

		public static readonly IAlgorithmFactory<DiffieHellmanGroup14SHA1KeyExchangeAlgorithm> Factory = AlgorithmFactory.For<DiffieHellmanGroup14SHA1KeyExchangeAlgorithm>(AlgorithmName);

		// The following steps are used to exchange a key.  In this, C is the
		// client; S is the server; p is a large safe prime; g is a generator
		// for a subgroup of GF(p); q is the order of the subgroup; V_S is S's
		// identification string; V_C is C's identification string; K_S is S's
		// public host key; I_C is C's SSH_MSG_KEXINIT message and I_S is S's
		// SSH_MSG_KEXINIT message that have been exchanged before this part
		// begins.

		readonly BigInteger p = DiffieHellmanGroups.MODPGroup2048Bit;
		readonly BigInteger g = new BigInteger(2);
		readonly BigInteger q = BigInteger.Pow(2, 640);  // TODO: what is the correct value of q? unfortunately I'm too ignorant in math to understand how to compute this value
		BigInteger xy;  // x if client, y if server

		public void Step1()
		{
			// 1. C generates a random number x (1 < x < q) and computes
			//    e = g ^ x mod p.  C sends e to S.

			throw new NotImplementedException();  // TODO
		}

		public void GenerateKeys(ProtocolVersion V_C, ProtocolVersion V_S, byte[] I_C, byte[] I_S, byte[] K_S, BigInteger e, byte[] sessionId, out BigInteger f, out byte[] H, int c2sIVLength, out byte[] c2sIV, int s2cIVLength, out byte[] s2cIV, int c2sKeyLength, out byte[] c2sKey, int s2cKeyLength, out byte[] s2cKey, int c2sIntegrityLength, out byte[] c2sIntegrity, int s2cIntegrityLength, out byte[] s2cIntegrity)
		{
			// Values of 'e' or 'f' that are not in the range [1, p-1] MUST NOT be
			// sent or accepted by either side.  If this condition is violated, the
			// key exchange fails.
			if (e < 1 || e > (p - 1))
				throw new ArgumentException("'e' must be in the range [1, p-1]", nameof(e));

			// 2. S generates a random number y (0 < y < q) and computes
			//    f = g^y mod p.  S receives e.  It computes K = e^y mod p,
			//    H = hash(V_C || V_S || I_C || I_S || K_S || e || f || K)
			//    (these elements are encoded according to their types; see below),
			//    and signature s on H with its private host key.  S sends
			//    (K_S || f || s) to C.  The signing operation may involve a
			//    second hashing operation.

			// generates a random number y (0 < y < q)
			using (var rng = RandomNumberGenerator.Create())
				xy = BigIntegerUtils.RandomInRange(rng, 0, q);

			// computes f = g^y mod p
			f = BigInteger.ModPow(g, xy, p);

			// computes K = e^y mod p
			var K = BigInteger.ModPow(e, xy, p);

			using (var sha1 = SHA1.Create())
			using (var buffer = ByteArrayPool.Acquire(PROTOCOL_VERSION_STRING_MAX_BYTES * 2 + I_C.Length + I_S.Length + K_S.Length + MAX_UNCOMPRESSED_PAYLOAD_SIZE * 4))  // conservative max size
			{
				// H = hash(V_C || V_S || I_C || I_S || K_S || e || f || K)
				var messageWriter = new SSHMessageWriter(buffer, 0);
				messageWriter.WriteString(V_C.IdentificationString, Encoding.UTF8);
				messageWriter.WriteString(V_S.IdentificationString, Encoding.UTF8);
				messageWriter.WriteBytes(I_C);
				messageWriter.WriteBytes(I_S);
				messageWriter.WriteBytes(K_S);
				messageWriter.WriteMPInt(e);
				messageWriter.WriteMPInt(f);
				messageWriter.WriteMPInt(K);
				H = sha1.ComputeHash(buffer, 0, messageWriter.MessageLength);

				// The exchange hash H from the first key exchange is
				// additionally used as the session identifier, which is a unique
				// identifier for this connection.
				if (sessionId == null)
					sessionId = H;

				// Initial IV client to server: HASH(K || H || "A" || session_id)
				// (Here K is encoded as mpint and "A" as byte and session_id as raw
				// data.  "A" means the single character A, ASCII 65).
				c2sIV = computeKey(sha1, buffer, c2sIVLength, K, H, (byte)'A', sessionId);

				// Initial IV server to client: HASH(K || H || "B" || session_id)
				s2cIV = computeKey(sha1, buffer, s2cIVLength, K, H, (byte)'B', sessionId);

				// Encryption key client to server: HASH(K || H || "C" || session_id)
				c2sKey = computeKey(sha1, buffer, c2sKeyLength, K, H, (byte)'C', sessionId);

				// Encryption key server to client: HASH(K || H || "D" || session_id)
				s2cKey = computeKey(sha1, buffer, s2cKeyLength, K, H, (byte)'D', sessionId);

				// Integrity key client to server: HASH(K || H || "E" || session_id)
				c2sIntegrity = computeKey(sha1, buffer, c2sIntegrityLength, K, H, (byte)'E', sessionId);

				// Integrity key server to client: HASH(K || H || "F" || session_id)
				s2cIntegrity = computeKey(sha1, buffer, s2cIntegrityLength, K, H, (byte)'F', sessionId);
			}
		}

		public void Step3()
		{
			// 3. C verifies that K_S really is the host key for S (e.g., using
			// certificates or a local database).  C is also allowed to accept
			// the key without verification; however, doing so will render the
			// protocol insecure against active attacks (but may be desirable for
			// practical reasons in the short term in many environments).  C then
			// computes K = f^x mod p, H = hash(V_C || V_S || I_C || I_S || K_S
			// || e || f || K), and verifies the signature s on H.

			throw new NotImplementedException();  // TODO
		}

		byte[] computeKey(SHA1 sha1, byte[] buffer, int keyLength, BigInteger K, byte[] H, byte X, byte[] sessionId)
		{
			// https://tools.ietf.org/html/rfc4253#section-7.2

			// Key data MUST be taken from the beginning of the hash output.  As
			// many bytes as needed are taken from the beginning of the hash value.
			// If the key length needed is longer than the output of the HASH, the
			// key is extended by computing HASH of the concatenation of K and H and
			// the entire key so far, and appending the resulting bytes(as many as
			// HASH generates) to the key.This process is repeated until enough
			// key material is available; the key is taken from the beginning of
			// this value.In other words:
			//
			// K1 = HASH(K || H || X || session_id)(X is e.g., "A")
			// K2 = HASH(K || H || K1)
			// K3 = HASH(K || H || K1 || K2)
			// ...
			// key = K1 || K2 || K3 || ...
			//
			// This process will lose entropy if the amount of entropy in K is
			// larger than the internal state size of HASH.

			var key = new byte[keyLength];
			var generatedBytes = 0;

			while (generatedBytes < keyLength)
			{
				var messageWriter = new SSHMessageWriter(buffer, 0);
				messageWriter.WriteMPInt(K);
				messageWriter.Write(H, 0, H.Length);

				if (generatedBytes == 0)
				{
					messageWriter.WriteByte(X);
					messageWriter.Write(sessionId, 0, sessionId.Length);
				}
				else
				{
					messageWriter.Write(key, 0, generatedBytes);
				}

				var hash = sha1.ComputeHash(buffer, 0, messageWriter.MessageLength);

				var count = Math.Min(hash.Length, key.Length - generatedBytes);
				hash.CopyTo(0, key, generatedBytes, count);
				generatedBytes += count;
			}

			return key;
		}
	}
}
