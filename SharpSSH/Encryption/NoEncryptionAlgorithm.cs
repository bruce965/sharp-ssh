﻿using SharpSSH.Util;

namespace SharpSSH.Encryption
{
	public class NoEncryptionAlgorithm : IEncryptionAlgorithm
	{
		public static readonly string AlgorithmName = "none";

		public static readonly NoEncryptionAlgorithm Instance = new NoEncryptionAlgorithm();

		public static readonly IAlgorithmFactory<NoEncryptionAlgorithm> Factory = AlgorithmFactory.For(AlgorithmName, Instance);

		public int BlockSize => 1;

		public int KeySize => 0;

		public void Encrypt(byte[] data, int dataOffset, int dataCount, byte[] encryptedData, int encryptedDataOffset)
		{
			data.CopyTo(dataOffset, encryptedData, encryptedDataOffset, dataCount);
		}

		public void Decrypt(byte[] data, int dataOffset, int dataCount, byte[] decryptedData, int decryptedDataOffset)
		{
			data.CopyTo(dataOffset, decryptedData, decryptedDataOffset, dataCount);
		}

		public void SetKey(byte[] key, int keyOffset, int keyLength, byte[] iv, int ivOffset, int ivLength) { }
	}
}
