﻿using System;

namespace SharpSSH
{
	public interface IAlgorithmFactory
	{
		string AlgorithmName { get; }
	}

	public interface IAlgorithmFactory<out T> : IAlgorithmFactory
	{
		T Instantiate();
	}
}
