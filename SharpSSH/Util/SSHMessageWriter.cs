﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace SharpSSH.Util
{
	/// <summary>Write values according to SSH specification.</summary>
	public class SSHMessageWriter
	{
		// https://tools.ietf.org/html/rfc4251#section-5

		readonly byte[] buffer;
		readonly int initialOffset;
		int position;

		public int MessageLength => position - initialOffset;

		public SSHMessageWriter(byte[] buffer, int offset)
		{
			this.buffer = buffer;
			initialOffset = offset;
			position = offset;
		}

		public void Write(byte[] bytes, int offset, int count)
		{
			for (var i = 0; i < count; i++)
			{
				buffer[position + i] = bytes[offset + i];
			}

			position += count;
		}

		public void WriteBoolean(bool value) => WriteByte(value ? (byte)1 : (byte)0);

		public void WriteByte(byte value)
		{
			buffer[position] = value;
			position += 1;
		}

		public void WriteBytes(byte[] value)
		{
			WriteUInt32((uint)value.Length);
			Write(value, 0, value.Length);
		}

		public void WriteString(string value, Encoding encoding)
		{
			int bytesCount = 0;
			if (value != null)
			{
				// NOTE: we leave a 4 bytes offset for the bytes count.
				bytesCount = encoding.GetBytes(value, 0, value.Length, buffer, position + 4);
			}

			WriteUInt32((uint)bytesCount);
			position += bytesCount;
		}

		public void WriteUInt32(uint value)
		{
			BitConverterBigEndian.FromUInt32(value, buffer, position);
			position += 4;
		}

		public void WriteNameList(IEnumerable<string> names) => WriteString(String.Join(",", from name in names select name.Replace(',', '?')), Encoding.ASCII);

		public void WriteMPInt(BigInteger value)
		{
			// https://msdn.microsoft.com/en-us/library/system.numerics.biginteger.tobytearray.aspx#Anchor_1
			// `ToByteArray` method retuns bytes in little-endian order
			var bytes = value.ToByteArray();

			WriteMPInt(bytes, true);
		}

		public void WriteMPInt(byte[] value, bool littleEndian)
		{
			// by spec, a value of 0 should result in a 0-length array, but `BigInteger` value 0 is represented as a 1-length array with first byte set to 0
			if (value.Length == 1 && value[0] == 0)
			{
				WriteUInt32(0);
				return;
			}

			WriteUInt32((uint)value.Length);

			if (littleEndian)
			{
				// in .NET, `BigInteger`s are represented in little-endian order, but SSH spec states that MSB must come first, we have to write bytes in reverse order
				for (var i = 0; i < value.Length; i++)
				{
					buffer[position + i] = value[value.Length - i - 1];
				}

				position += value.Length;
			}
			else
			{
				// if `value` is already big-endian, we write bytes in the original order
				Write(value, 0, value.Length);
			}
		}
	}
}
