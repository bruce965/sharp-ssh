﻿using SharpSSH.Compression;
using SharpSSH.Encryption;
using SharpSSH.KeyExchange;
using SharpSSH.MAC;
using SharpSSH.Message;
using SharpSSH.ServerHostKey;
using SharpSSH.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH
{
	partial class SSHServer
	{
		partial class RemotePeer
		{
			/// <summary>Current client ↔ server echange context, should only be used inside a <c>lock</c> over itself.</summary>
			public readonly ExchangeContext ExchangeContext = new ExchangeContext();

			/// <summary>Temporary incomplete exchange context created during a key exchange, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public ExchangeContext PendingExchangeContext;

			/// <summary>Used during first Diffie-Hellman Key Exchange initialization, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public byte[] SessionId;

#pragma warning disable IDE1006 // Naming Styles

			/// <summary>Used during Diffie-Hellman Key Exchange initialization, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public byte[] I_C;
			/// <summary>Used during Diffie-Hellman Key Exchange initialization, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public byte[] I_S;
			/// <summary>Used during Diffie-Hellman Key Exchange initialization, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public byte[] K_S;
			/// <summary>Used during Diffie-Hellman Key Exchange initialization, should only be used inside a <c>lock</c> over <see cref="ExchangeContext"/>.</summary>
			public BigInteger e;

#pragma warning restore IDE1006 // Naming Styles

			public string Language
			{
				get
				{
					lock (ExchangeContext)
						return ExchangeContext.LocalToRemoteLanguage;
				}
			}
		}

		protected class ExchangeContext
		{
			public IKeyExchangeAlgorithm KeyExchangeAlgorithm { get; set; } = null;

			public IServerHostKeyAlgorithm ServerHostKeyAlgorithm { get; set; } = null;

			public IEncryptionAlgorithm RemoteToLocalEncryptionAlgorithm { get; set; } = NoEncryptionAlgorithm.Instance;
			public IEncryptionAlgorithm LocalToRemoteEncryptionAlgorithm { get; set; } = NoEncryptionAlgorithm.Instance;

			public IMACAlgorithm RemoteToLocalMACAlgorithm { get; set; } = NoMACAlgorithm.Instance;
			public IMACAlgorithm LocalToRemoteMACAlgorithm { get; set; } = NoMACAlgorithm.Instance;

			public ICompressionAlgorithm RemoteToLocalCompressionAlgorithm { get; set; } = NoCompressionAlgorithm.Instance;
			public ICompressionAlgorithm LocalToRemoteCompressionAlgorithm { get; set; } = NoCompressionAlgorithm.Instance;

			public string RemoteToLocalLanguage { get; set; } = null;
			public string LocalToRemoteLanguage { get; set; } = null;

			public uint NextRemoteToLocalMessageSequenceNumber { get; set; } = 0;
			public uint NextLocalToRemoteMessageSequenceNumber { get; set; } = 0;
		}

		/// <summary>Supported key exchange algorithms, sorted by preference.</summary>
		public IList<IAlgorithmFactory<IKeyExchangeAlgorithm>> SupportedKeyExchangeAlgorithms { get; } = new List<IAlgorithmFactory<IKeyExchangeAlgorithm>>()
		{
			DiffieHellmanGroup14SHA1KeyExchangeAlgorithm.Factory
		};

		/// <summary>Supported server host key algorithms, sorted by preference.</summary>
		public IList<IAlgorithmFactory<IServerHostKeyAlgorithm>> SupportedServerHostKeyAlgorithms { get; } = new List<IAlgorithmFactory<IServerHostKeyAlgorithm>>();

		/// <summary>Supported encryption algorithms, sorted by preference.</summary>
		public IList<IAlgorithmFactory<IEncryptionAlgorithm>> SupportedEncryptionAlgorithms { get; } = new List<IAlgorithmFactory<IEncryptionAlgorithm>>()
		{
			TDESCBCEncryptionAlgorithm.Factory,
			NoEncryptionAlgorithm.Factory
		};

		/// <summary>Supported MAC algorithms, sorted by preference.</summary>
		public IList<IAlgorithmFactory<IMACAlgorithm>> SupportedMacAlgorithms { get; } = new List<IAlgorithmFactory<IMACAlgorithm>>()
		{
			HMACSHA1MACAlgorithm.Factory,
			NoMACAlgorithm.Factory
		};

		/// <summary>Supported compression algorithms, sorted by preference.</summary>
		public IList<IAlgorithmFactory<ICompressionAlgorithm>> SupportedCompressionAlgorithms { get; } = new List<IAlgorithmFactory<ICompressionAlgorithm>>()
		{
			NoCompressionAlgorithm.Factory
		};

		/// <summary>Preferred languages for local user, sorted by preference.</summary>
		public IList<string> PreferredLanguages { get; } = new List<string>();

		/// <summary>Available languages for remote user, sorted by preference.</summary>
		public IList<string> AvailableLanguages { get; } = new List<string>();

		T getPreferredAlgorithm<T>(IEnumerable<string> candidates, IEnumerable<IAlgorithmFactory<T>> supported) where T : class
		{
			return supported.FirstOrDefault(algo => candidates.Contains(algo.AlgorithmName))?.Instantiate();
		}

		string getPreferredLanguage(IEnumerable<string> candidates, IEnumerable<string> supported)
		{
			return supported.FirstOrDefault(lang => candidates.Contains(lang));
		}

		protected virtual Task ProcessKeyExchangeInitializationMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			KeyExchangeInitializationMessage.Read(
				message, messageOffset, messageLength,
				out byte[] keyExchangeCookie,
				out string[] keyExchangeAlgorithms,
				out string[] serverHostKeyAlgorithms,
				out string[] encryptionAlgorithmsC2S, out string[] encryptionAlgorithmsS2C,
				out string[] macAlgorithmsC2S, out string[] macAlgorithmsS2C,
				out string[] compressionAlgorithmsC2S, out string[] compressionAlgorithmsS2C,
				out string[] languagesC2S, out string[] languagesS2C,
				out bool firstKexPacketFollows
			);

			lock (remotePeer.ExchangeContext)
			{
				if (remotePeer.PendingExchangeContext != null)
				{
					// TODO: should we fail if a key exchange is already in-progress or should we just start a new key exchange instead?
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, "A key exchange is already pending", null);
					return Task.CompletedTask;
				}

				remotePeer.PendingExchangeContext = new ExchangeContext
				{
					KeyExchangeAlgorithm = getPreferredAlgorithm(keyExchangeAlgorithms, SupportedKeyExchangeAlgorithms),
					ServerHostKeyAlgorithm = getPreferredAlgorithm(serverHostKeyAlgorithms, SupportedServerHostKeyAlgorithms),
					RemoteToLocalEncryptionAlgorithm = getPreferredAlgorithm(isServer ? encryptionAlgorithmsC2S : encryptionAlgorithmsS2C, SupportedEncryptionAlgorithms),
					LocalToRemoteEncryptionAlgorithm = getPreferredAlgorithm(isServer ? encryptionAlgorithmsS2C : encryptionAlgorithmsC2S, SupportedEncryptionAlgorithms),
					RemoteToLocalMACAlgorithm = getPreferredAlgorithm(isServer ? macAlgorithmsC2S : macAlgorithmsS2C, SupportedMacAlgorithms),
					LocalToRemoteMACAlgorithm = getPreferredAlgorithm(isServer ? macAlgorithmsS2C : macAlgorithmsC2S, SupportedMacAlgorithms),
					RemoteToLocalCompressionAlgorithm = getPreferredAlgorithm(isServer ? compressionAlgorithmsC2S : compressionAlgorithmsS2C, SupportedCompressionAlgorithms),
					LocalToRemoteCompressionAlgorithm = getPreferredAlgorithm(isServer ? compressionAlgorithmsS2C : compressionAlgorithmsC2S, SupportedCompressionAlgorithms),
					RemoteToLocalLanguage = getPreferredLanguage(isServer ? languagesC2S : languagesS2C, PreferredLanguages),
					LocalToRemoteLanguage = getPreferredLanguage(isServer ? languagesS2C : languagesC2S, PreferredLanguages)
				};

				if (
					remotePeer.PendingExchangeContext.KeyExchangeAlgorithm == null ||
					remotePeer.PendingExchangeContext.ServerHostKeyAlgorithm == null ||
					remotePeer.PendingExchangeContext.RemoteToLocalEncryptionAlgorithm == null ||
					remotePeer.PendingExchangeContext.LocalToRemoteEncryptionAlgorithm == null ||
					remotePeer.PendingExchangeContext.RemoteToLocalMACAlgorithm == null ||
					remotePeer.PendingExchangeContext.LocalToRemoteMACAlgorithm == null ||
					remotePeer.PendingExchangeContext.RemoteToLocalCompressionAlgorithm == null ||
					remotePeer.PendingExchangeContext.LocalToRemoteCompressionAlgorithm == null
				)
				{
					remotePeer.Disconnect(DisconnectionReason.KeyExchangeFailed, "Key exchange failed: failed to come to an agreement over algorithms", null);
					remotePeer.PendingExchangeContext = null;
					return Task.CompletedTask;
				}

				if (this.isServer)
					remotePeer.I_C = message.Slice(messageOffset, messageLength);
				else
					remotePeer.I_S = message.Slice(messageOffset, messageLength);
			}

			return Task.CompletedTask;
		}

		protected virtual async Task ProcessDiffieHellmanKeyExchangeInitializationMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			// https://tools.ietf.org/html/rfc4253#section-8

			IKeyExchangeAlgorithm keyExchange;
			IServerHostKeyAlgorithm serverHostKey;
			IEncryptionAlgorithm localToRemoteEcryptionAlgorithm;
			IEncryptionAlgorithm remoteToLocalEcryptionAlgorithm;
			IMACAlgorithm localToRemoteMacAlgorithm;
			IMACAlgorithm remoteToLocalMacAlgorithm;
			byte[] sessionId;

			// NOTE: `PedingExchangeContext` must always be used with a `lock` over `ExchangeContext`.
			lock (remotePeer.ExchangeContext)
			{
				// if we are not performing a key exchange, this message cannot be processed
				if (remotePeer.PendingExchangeContext == null)
				{
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, "SSH_MSG_KEXDH_INIT must be preceeded by a SSH_MSG_KEXINIT", null);
					return;
				}

				keyExchange = remotePeer.PendingExchangeContext.KeyExchangeAlgorithm;
				serverHostKey = remotePeer.PendingExchangeContext.ServerHostKeyAlgorithm;
				localToRemoteEcryptionAlgorithm = remotePeer.PendingExchangeContext.LocalToRemoteEncryptionAlgorithm;
				remoteToLocalEcryptionAlgorithm = remotePeer.PendingExchangeContext.RemoteToLocalEncryptionAlgorithm;
				localToRemoteMacAlgorithm = remotePeer.PendingExchangeContext.LocalToRemoteMACAlgorithm;
				remoteToLocalMacAlgorithm = remotePeer.PendingExchangeContext.RemoteToLocalMACAlgorithm;
				sessionId = remotePeer.SessionId;
			}

			DiffieHellmanKeyExchangeInitializationMessage.Read(message, messageOffset, messageLength, out BigInteger e);

			byte[] K_S;
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var keyAndCertificatesLength = serverHostKey.CreateKeyAndCertificatesData(buffer.Array, 0);
				K_S = buffer.Array.Slice(0, keyAndCertificatesLength);
			}

			byte[] I_C;
			byte[] I_S;

			// NOTE: `K_S`, `e`, `I_C` and `I_S` must always be used with a `lock` over `ExchangeContext`.
			lock (remotePeer.ExchangeContext)
			{
				remotePeer.K_S = K_S;
				remotePeer.e = e;

				I_C = remotePeer.I_C;
				I_S = remotePeer.I_S;
			}

			keyExchange.GenerateKeys(
				remotePeer.ProtocolVersion, ProtocolVersion, I_C, I_S, K_S, e, sessionId, out var f, out var H,
				(isServer ? remoteToLocalEcryptionAlgorithm : localToRemoteEcryptionAlgorithm).BlockSize, out var c2sIV,
				(isServer ? localToRemoteEcryptionAlgorithm : remoteToLocalEcryptionAlgorithm).BlockSize, out var s2cIV,
				(isServer ? remoteToLocalEcryptionAlgorithm : localToRemoteEcryptionAlgorithm).KeySize, out var c2sKey,
				(isServer ? localToRemoteEcryptionAlgorithm : remoteToLocalEcryptionAlgorithm).KeySize, out var s2cKey,
				(isServer ? remoteToLocalMacAlgorithm : localToRemoteMacAlgorithm).KeySize, out var c2sIntegrity,
				(isServer ? localToRemoteMacAlgorithm : remoteToLocalMacAlgorithm).KeySize, out var s2cIntegrity
			);

			byte[] signatureOfH;
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var signatureLength = serverHostKey.CreateSignatureData(H, buffer.Array, 0);
				signatureOfH = buffer.Array.Slice(0, signatureLength);
			}

			await remotePeer.SendDiffieHellmanKeyExchangeReplyMessage(K_S, f, signatureOfH);

			// NOTE: `PedingExchangeContext` must always be used with a `lock` over `ExchangeContext`.
			lock (remotePeer.ExchangeContext)
			{
				// set the generated keys
				if (isServer)
				{
					remotePeer.PendingExchangeContext.LocalToRemoteEncryptionAlgorithm.SetKey(s2cKey, 0, s2cKey.Length, s2cIV, 0, s2cIV.Length);
					remotePeer.PendingExchangeContext.RemoteToLocalEncryptionAlgorithm.SetKey(c2sKey, 0, c2sKey.Length, c2sIV, 0, c2sIV.Length);
					remotePeer.PendingExchangeContext.LocalToRemoteMACAlgorithm.SetKey(s2cIntegrity, 0, s2cIntegrity.Length);
					remotePeer.PendingExchangeContext.RemoteToLocalMACAlgorithm.SetKey(c2sIntegrity, 0, c2sIntegrity.Length);
				}
				else
				{
					remotePeer.PendingExchangeContext.LocalToRemoteEncryptionAlgorithm.SetKey(c2sKey, 0, c2sKey.Length, c2sIV, 0, c2sIV.Length);
					remotePeer.PendingExchangeContext.RemoteToLocalEncryptionAlgorithm.SetKey(s2cKey, 0, s2cKey.Length, s2cIV, 0, s2cIV.Length);
					remotePeer.PendingExchangeContext.LocalToRemoteMACAlgorithm.SetKey(c2sIntegrity, 0, c2sIntegrity.Length);
					remotePeer.PendingExchangeContext.RemoteToLocalMACAlgorithm.SetKey(s2cIntegrity, 0, s2cIntegrity.Length);
				}

				// The exchange hash H from the first key exchange is
				// additionally used as the session identifier, which is a unique
				// identifier for this connection.
				if (remotePeer.SessionId == null)
					remotePeer.SessionId = H;
			}

			await remotePeer.SendNewKeysMessage();
		}

		protected virtual Task ProcessDiffieHellmanKeyExchangeReplyMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			throw new NotImplementedException();  // TODO
		}

		protected virtual Task ProcessNewKeysMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			lock (remotePeer.ExchangeContext)
			{
				// if we are not performing a key exchange, this message cannot be processed
				if (remotePeer.PendingExchangeContext == null)
				{
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, "SSH_MSG_NEWKEYS must be preceeded by a SSH_MSG_KEXDH_INIT", null);
					return Task.CompletedTask;
				}

				// transfer pending keys and algorithms to the in-use context
				remotePeer.ExchangeContext.KeyExchangeAlgorithm = remotePeer.PendingExchangeContext.KeyExchangeAlgorithm;
				remotePeer.ExchangeContext.ServerHostKeyAlgorithm = remotePeer.PendingExchangeContext.ServerHostKeyAlgorithm;
				remotePeer.ExchangeContext.RemoteToLocalEncryptionAlgorithm = remotePeer.PendingExchangeContext.RemoteToLocalEncryptionAlgorithm;
				remotePeer.ExchangeContext.LocalToRemoteEncryptionAlgorithm = remotePeer.PendingExchangeContext.LocalToRemoteEncryptionAlgorithm;
				remotePeer.ExchangeContext.RemoteToLocalMACAlgorithm = remotePeer.PendingExchangeContext.RemoteToLocalMACAlgorithm;
				remotePeer.ExchangeContext.LocalToRemoteMACAlgorithm = remotePeer.PendingExchangeContext.LocalToRemoteMACAlgorithm;
				remotePeer.ExchangeContext.RemoteToLocalCompressionAlgorithm = remotePeer.PendingExchangeContext.RemoteToLocalCompressionAlgorithm;
				remotePeer.ExchangeContext.LocalToRemoteCompressionAlgorithm = remotePeer.PendingExchangeContext.LocalToRemoteCompressionAlgorithm;
				remotePeer.ExchangeContext.RemoteToLocalLanguage = remotePeer.PendingExchangeContext.RemoteToLocalLanguage;
				remotePeer.ExchangeContext.LocalToRemoteLanguage = remotePeer.PendingExchangeContext.LocalToRemoteLanguage;

				remotePeer.I_C = null;
				remotePeer.I_S = null;
				remotePeer.K_S = null;
				remotePeer.e = BigInteger.Zero;

				remotePeer.PendingExchangeContext = null;
			}

			return Task.CompletedTask;
		}
	}
}
