﻿using System;

namespace SharpSSH.Encryption
{
	public interface IEncryptionAlgorithm
	{
		int BlockSize { get; }

		int KeySize { get; }

		void Encrypt(byte[] data, int dataOffset, int dataCount, byte[] encryptedData, int encryptedDataOffset);

		void Decrypt(byte[] data, int dataOffset, int dataCount, byte[] decryptedData, int decryptedDataOffset);

		void SetKey(byte[] key, int keyOffset, int keyLength, byte[] iv, int ivOffset, int ivLength);
	}
}
