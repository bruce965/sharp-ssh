﻿using System;
using System.Numerics;
using System.Text;

namespace SharpSSH.Util
{
	/// <summary>Read values according to SSH specification.</summary>
	public class SSHMessageReader
	{
		// https://tools.ietf.org/html/rfc4251#section-5

		static readonly char[] nameListSeparators = new[] { ',' };

		readonly byte[] buffer;
		readonly int initialOffset;
		int position;

		public int MessageLength => position - initialOffset;

		public SSHMessageReader(byte[] buffer, int offset)
		{
			this.buffer = buffer;
			initialOffset = offset;
			position = offset;
		}

		public void Read(byte[] bytes, int offset, int count)
		{
			for (var i = 0; i < count; i++)
			{
				bytes[offset + i] = buffer[position + i];
			}

			position += count;
		}

		public bool ReadBoolean() => ReadByte() != 0;

		public byte ReadByte()
		{
			var value = buffer[position];
			position += 1;
			return value;
		}

		public byte[] ReadBytes()
		{
			var bytesCount = (int)ReadUInt32();
			if (position + bytesCount > buffer.Length)
				throw new IndexOutOfRangeException("Trying to read a more bytes than the available data");  // prevent huge alloc

			var value = new byte[bytesCount];
			Read(value, 0, bytesCount);
			return value;
		}

		public string ReadString(Encoding encoding)
		{
			var bytesCount = ReadUInt32();
			var value = encoding.GetString(buffer, position, (int)bytesCount);
			position += (int)bytesCount;
			return value;
		}

		public uint ReadUInt32()
		{
			var value = BitConverterBigEndian.ToUInt32(buffer, position);
			position += 4;
			return value;
		}

		public string[] ReadNameList() => ReadString(Encoding.ASCII).Split(nameListSeparators, StringSplitOptions.RemoveEmptyEntries);

		public BigInteger ReadMPInt()
		{
			var bytes = ReadBytes();

			// https://msdn.microsoft.com/en-us/library/dd268207.aspx#Anchor_2
			// `BigInteger` constructor always takes bytes in little-endian order, but SSH spec states that MSB must come first, we have to reverse bytes
			bytes.ReverseInPlace();

			// NOTE: except for the endian-ness, the `BigInteger` constructor already matches with all other SSH specs for MPInt type
			// we don't even have to remove the leading zero if the sign big would be set for a positive integer.

			return new BigInteger(bytes);
		}
	}
}
