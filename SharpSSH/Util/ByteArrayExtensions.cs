﻿using System;

namespace SharpSSH.Util
{
	public static class ByteArrayExtensions
	{
		public static void CopyTo(this byte[] source, int sourceOffset, byte[] destination, int destinationOffset, int count)
		{
			for (var i = 0; i < count; i++)
			{
				destination[destinationOffset + i] = source[sourceOffset + i];
			}
		}

		public static bool RangeEquals(this byte[] array1, int offset1, byte[] array2, int offset2, int count)
		{
			for (var i = 0; i < count; i++)
			{
				if (array1[offset1 + i] != array2[offset2 + i])
					return false;
			}

			return true;
		}

		public static void ReverseInPlace(this byte[] array, int offset, int count)
		{
			var middle = offset + (int)(count * 0.5);
			var end = offset + count - 1;

			for (var i = 0; i < middle; i++)
			{
				var buffer = array[i];
				array[i] = array[end - i];
				array[end - i] = buffer;
			}
		}

		public static byte[] ReverseInPlace(this byte[] array)
		{
			ReverseInPlace(array, 0, array.Length);
			return array;
		}

		public static byte[] Slice(this byte[] array, int offset, int count)
		{
			var slice = new byte[count];
			CopyTo(array, offset, slice, 0, count);
			return slice;
		}
	}
}
