﻿using System;
using System.Text;

namespace SharpSSH
{
	// https://tools.ietf.org/html/rfc4250#section-4.1.2
	public enum MessageType : Byte
	{
		/// <summary>SSH_MSG_DISCONNECT</summary>
		Disconnect = 1,
		/// <summary>SSH_MSG_IGNORE</summary>
		Ignore = 2,
		/// <summary>SSH_MSG_UNIMPLEMENTED</summary>
		Unimplemented = 3,
		/// <summary>SSH_MSG_DEBUG</summary>
		Debug = 4,
		/// <summary>SSH_MSG_SERVICE_REQUEST</summary>
		ServiceRequest = 5,
		/// <summary>SSH_MSG_SERVICE_ACCEPT</summary>
		ServiceAccept = 6,
		/// <summary>SSH_MSG_KEXINIT</summary>
		KeyExchangeInitialization = 20,
		/// <summary>SSH_MSG_NEWKEYS</summary>
		NewKeys = 21,

		/// <summary>SSH_MSG_KEXDH_INIT</summary>
		DiffieHellmanKeyExchangeInitialization = 30,
		/// <summary>SSH_MSG_KEXDH_REPLY</summary>
		DiffieHellmanKeyExchangeReply = 31,

		/// <summary>SSH_MSG_USERAUTH_REQUEST</summary>
		AuthenticationRequest = 50,
		/// <summary>SSH_MSG_USERAUTH_FAILURE</summary>
		AuthenticationFailure = 51,
		/// <summary>SSH_MSG_USERAUTH_SUCCESS</summary>
		AuthenticationSuccess = 52,
		/// <summary>SSH_MSG_USERAUTH_BANNER</summary>
		AuthenticationBanner = 53,

		/// <summary>SSH_MSG_GLOBAL_REQUEST</summary>
		SSH_MSG_GLOBAL_REQUEST = 80,
		/// <summary>SSH_MSG_REQUEST_SUCCESS</summary>
		SSH_MSG_REQUEST_SUCCESS = 81,
		/// <summary>SSH_MSG_REQUEST_FAILURE</summary>
		SSH_MSG_REQUEST_FAILURE = 82,
		/// <summary>SSH_MSG_CHANNEL_OPEN</summary>
		SSH_MSG_CHANNEL_OPEN = 90,
		/// <summary>SSH_MSG_CHANNEL_OPEN_CONFIRMATION</summary>
		SSH_MSG_CHANNEL_OPEN_CONFIRMATION = 91,
		/// <summary>SSH_MSG_CHANNEL_OPEN_FAILURE</summary>
		SSH_MSG_CHANNEL_OPEN_FAILURE = 92,
		/// <summary>SSH_MSG_CHANNEL_WINDOW_ADJUST</summary>
		SSH_MSG_CHANNEL_WINDOW_ADJUST = 93,
		/// <summary>SSH_MSG_CHANNEL_DATA</summary>
		SSH_MSG_CHANNEL_DATA = 94,
		/// <summary>SSH_MSG_CHANNEL_EXTENDED_DATA</summary>
		SSH_MSG_CHANNEL_EXTENDED_DATA = 95,
		/// <summary>SSH_MSG_CHANNEL_EOF</summary>
		SSH_MSG_CHANNEL_EOF = 96,
		/// <summary>SSH_MSG_CHANNEL_CLOSE</summary>
		SSH_MSG_CHANNEL_CLOSE = 97,
		/// <summary>SSH_MSG_CHANNEL_REQUEST</summary>
		SSH_MSG_CHANNEL_REQUEST = 98,
		/// <summary>SSH_MSG_CHANNEL_SUCCESS</summary>
		SSH_MSG_CHANNEL_SUCCESS = 99,
		/// <summary>SSH_MSG_CHANNEL_FAILURE</summary>
		SSH_MSG_CHANNEL_FAILURE = 100
	}

	public static class MessageTypeExtensions
	{
		/// <summary>Get a string representation of this message type.</summary>
		/// <param name="type">Message type.</param>
		/// <returns>Official representation of the message type, or a fallback representation for custom messages.</returns>
		public static string GetName(this MessageType type)
		{
			var sb = new StringBuilder(64);
			sb.Append("SSH_MSG_");

			// class name
			switch ((int)type)
			{
				// invalid
				case var n when n < 1:
					sb.Append("CUSTOM_");
					sb.Append(n);
					break;

				// transport
				case var n when n < 50:
					switch (n)
					{
						case 1:
							sb.Append("DISCONNECT");
							break;
						case 2:
							sb.Append("IGNORE");
							break;
						case 3:
							sb.Append("UNIMPLEMENTED");
							break;
						case 4:
							sb.Append("DEBUG");
							break;
						case 5:
							sb.Append("SERVICE_REQUEST");
							break;
						case 6:
							sb.Append("SERVICE_ACCEPT");
							break;
						case 20:
							sb.Append("KEXINIT");
							break;
						case 21:
							sb.Append("NEWKEYS");
							break;
						case 30:
							sb.Append("KEXDH_INIT");
							break;
						case 31:
							sb.Append("KEXDH_REPLY");
							break;
						default:
							sb.Append("TRANS_CUSTOM_");
							sb.Append(n);
							break;
					}
					break;

				// authentication
				case var n when n < 80:
					sb.Append("USERAUTH_");
					switch (n)
					{
						case 50:
							sb.Append("REQUEST");
							break;
						case 51:
							sb.Append("FAILURE");
							break;
						case 52:
							sb.Append("SUCCESS");
							break;
						case 53:
							sb.Append("BANNER");
							break;
						default:
							sb.Append("CUSTOM_");
							sb.Append(n);
							break;
					}
					break;

				// connection
				case var n when n <= 100:
					switch (n)
					{
						case 80:
							sb.Append("GLOBAL_REQUEST");
							break;
						case 81:
							sb.Append("REQUEST_SUCCESS");
							break;
						case 82:
							sb.Append("REQUEST_FAILURE");
							break;
						case var m when m >= 90:
							sb.Append("CHANNEL_");
							switch (m)
							{
								case 90:
									sb.Append("OPEN");
									break;
								case 91:
									sb.Append("OPEN_CONFIRMATION");
									break;
								case 92:
									sb.Append("OPEN_FAILURE");
									break;
								case 93:
									sb.Append("WINDOW_ADJUST");
									break;
								case 94:
									sb.Append("DATA");
									break;
								case 95:
									sb.Append("EXTENDED_DATA");
									break;
								case 96:
									sb.Append("EOF");
									break;
								case 97:
									sb.Append("CLOSE");
									break;
								case 98:
									sb.Append("REQUEST");
									break;
								case 99:
									sb.Append("SUCCESS");
									break;
								case 100:
									sb.Append("FAILURE");
									break;
							}
							break;
						default:
							sb.Append("CONNECT_CUSTOM_");
							sb.Append(n);
							break;
					}
					break;

				default:
					sb.Append("CUSTOM_");
					sb.Append((int)type);
					break;
			}

			return sb.ToString();
		}
	}
}
