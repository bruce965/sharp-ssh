﻿using System;

namespace SharpSSH
{
	public static class SSHConstants
	{
		// https://tools.ietf.org/html/rfc4253#section-4.2
		public const string PROTO_VERSION = "2.0";
		public const int PROTOCOL_VERSION_STRING_MAX_CHARACTERS = 255;
		public const int PROTOCOL_VERSION_STRING_MAX_BYTES = PROTOCOL_VERSION_STRING_MAX_CHARACTERS * 4;  // actually its's a bit less, but who really cares?

		// https://tools.ietf.org/html/rfc4253#section-6
		public const int MIN_PACKET_SIZE = 16;
		public const int MIN_PADDING_BYTES = 4;

		// https://tools.ietf.org/html/rfc4253#section-6.1
		public const int MAX_PACKET_SIZE = 35000;
		public const int MAX_UNCOMPRESSED_PAYLOAD_SIZE = 32768;
	}
}
