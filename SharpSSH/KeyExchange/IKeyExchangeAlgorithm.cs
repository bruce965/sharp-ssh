﻿using System.Numerics;

namespace SharpSSH.KeyExchange
{
	public interface IKeyExchangeAlgorithm
	{
		void Step1();
		void GenerateKeys(ProtocolVersion V_C, ProtocolVersion V_S, byte[] I_C, byte[] I_S, byte[] K_S, BigInteger e, byte[] sessionId, out BigInteger f, out byte[] H, int c2sIVLength, out byte[] c2sIV, int s2cIVLength, out byte[] s2cIV, int c2sKeyLength, out byte[] c2sKey, int s2cKeyLength, out byte[] s2cKey, int c2sIntegrityLength, out byte[] c2sIntegrity, int s2cIntegrityLength, out byte[] s2cIntegrity);
		void Step3();
	}
}
