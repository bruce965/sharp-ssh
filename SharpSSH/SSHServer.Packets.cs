﻿using SharpSSH.Compression;
using SharpSSH.Encryption;
using SharpSSH.MAC;
using SharpSSH.Message;
using SharpSSH.Util;
using System;
using System.Threading;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH
{
	partial class SSHServer
	{
		partial class RemotePeer
		{
			public delegate Task SendMessageDelegate(byte[] data, int offset, int length);

			readonly SemaphoreSlim sendMessageSemaphore = new SemaphoreSlim(1, 1);
			readonly SendMessageDelegate sendMessage;

			public Task SendMessage(byte[] data, int offset, int length) => SendMessage(data, offset, length, cts.Token);

			/// <summary>
			/// Send a message to this client.
			/// 
			/// Unlike with <see cref="SendMessage(byte[], int, int)"/>, the message will be sent even if the client is disconnecting.
			/// </summary>
			/// <param name="data">Content of the message.</param>
			/// <param name="offset">Offset of the message inside <paramref name="data"/>.</param>
			/// <param name="length">Length of the message in bytes.</param>
			/// <param name="cancellationToken">Cancellation token used to interrupt the operation from the outside.</param>
			/// <returns>Task resolved after message is sent.</returns>
			public async Task SendMessage(byte[] data, int offset, int length, CancellationToken cancellationToken)
			{
				// it is not possible to send multiple messages at the same time, we use a semaphore to coordinate
				await sendMessageSemaphore.WaitAsync(cancellationToken);

				try
				{
					await sendMessage(data, offset, length);
				}
				finally
				{
					// release the semaphore after sending the message
					sendMessageSemaphore.Release();
				}
			}
		}

		#region Events

		public delegate void MessageEventHandler(SSHServer source, IRemotePeer remotePeer, byte[] data, int offset, int length, uint messageSequenceNumber);

		/// <summary>An SSH message has been received.</summary>
		public event SSHServerEvent<MessageEventArgs> MessageReceived;

		/// <summary>An SSH message is being sent.</summary>
		public event SSHServerEvent<MessageEventArgs> SendingMessage;

		/// <summary>A debug message has been received.</summary>
		public event SSHServerEvent<DebugMessageEventArgs> DebugMessageReceived;

		/// <summary>Raise the <see cref="MessageReceived"/> event to report that an SSH message has been received.</summary>
		protected virtual void RaiseMessageReceived(IRemotePeer remotePeer, byte[] data, int offset, int length, uint messageSequenceNumber)
		{
			if (MessageReceived == null)
				return;

			// NOTE: we use a factory to generate the args, because they contain a stream which must be recreated for every handler.
			raiseSafely(MessageReceived, () => new MessageEventArgs(remotePeer, data, offset, length, messageSequenceNumber));
		}

		/// <summary>Raise the <see cref="SendingMessage"/> event to report that an SSH message is being sent.</summary>
		protected virtual void RaiseSendingMessage(IRemotePeer remotePeer, byte[] data, int offset, int length, uint messageSequenceNumber)
		{
			if (SendingMessage == null)
				return;

			// NOTE: we use a factory to generate the args, because they contain a stream which must be recreated for every handler.
			raiseSafely(SendingMessage, () => new MessageEventArgs(remotePeer, data, offset, length, messageSequenceNumber));
		}

		/// <summary>Raise the <see cref="DebugMessageReceived"/> event to report that a debug message has been received.</summary>
		protected virtual void RaiseDebugMessageReceived(IRemotePeer remotePeer, bool alwaysDisplay, string message, string language)
		{
			if (DebugMessageReceived == null)
				return;

			raiseSafely(DebugMessageReceived, new DebugMessageEventArgs(remotePeer, alwaysDisplay, message, language));
		}

		#endregion

		/// <summary>Read, parse and process a packet from the TCP stream.</summary>
		/// <param name="exchangeContext">Exchange context with informations used to communicate with peer. Must only be used with a `lock` over itself.</param>
		/// <param name="socketStream">TCP socket stream.</param>
		/// <param name="cancellationToken">Cancellation token used to interrupt the operation from the outside.</param>
		/// <returns>Task resolved after message is processed.</returns>
		async Task readPacket(RemotePeer remotePeer, CancellationToken cancellationToken)
		{
			// https://github.com/TyrenDe/SSHServer/wiki/008:-Reading-a-Packet

			// PACKET FORMAT:
			// <encrypted>
			//   <mac>
			//     uint32 packet_length
			//     <packet_length>
			//       byte padding_length
			//       <compressed>
			//         byte[n1] payload; n1 = packet_length - padding_length - 1
			//       </compressed>
			//       byte[n2] random padding; n2 = padding_length
			//     </packet_length>
			//   </mac>
			// </encrypted>
			// byte[m] mac(Message Authentication Code -MAC); m = mac_length

			using (var rawPacketBuffer = ByteArrayPool.Acquire(MAX_PACKET_SIZE))
			using (var decryptedBuffer = ByteArrayPool.Acquire(MAX_PACKET_SIZE))
			using (var decompressedBuffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				uint messageSequenceNumber;
				IEncryptionAlgorithm encryption;
				IMACAlgorithm mac;
				ICompressionAlgorithm compression;

				// NOTE: `ExchangeContext` must always be used with a `lock` over itself.
				lock (remotePeer.ExchangeContext)
				{
					// read and increment message sequence number
					messageSequenceNumber = unchecked(remotePeer.ExchangeContext.NextRemoteToLocalMessageSequenceNumber++);

					encryption = remotePeer.ExchangeContext.RemoteToLocalEncryptionAlgorithm;
					mac = remotePeer.ExchangeContext.RemoteToLocalMACAlgorithm;
					compression = remotePeer.ExchangeContext.RemoteToLocalCompressionAlgorithm;
				}

				// first block size = max(cipher block size, 8 bytes), as stated in https://tools.ietf.org/html/rfc4253#section-6
				var blockSize = (int)Math.Ceiling(8d / encryption.BlockSize) * encryption.BlockSize;

				// read the first block
				var read = await remotePeer.Stream.ReadAtLeast(rawPacketBuffer.Array, 0, blockSize, Math.Max(MIN_PACKET_SIZE, blockSize), cancellationToken);
				if (read < blockSize)
				{
					remotePeer.Disconnect(DisconnectionReason.ConnectionLost);
					return;
				}

				// decrypt the first piece of the packet
				encryption.Decrypt(rawPacketBuffer.Array, 0, blockSize, decryptedBuffer.Array, 0);
				var decrypted = blockSize;

				// read packet length and padding length off the decrypted value
				var packetLength = BitConverterBigEndian.ToUInt32(decryptedBuffer.Array, 0);
				var paddingLength = decryptedBuffer.Array[4];
				var totalPacketLength = (int)packetLength + mac.DigestLength + 4;
				var totalEncryptedLength = totalPacketLength - mac.DigestLength;
				var totalCompressedLength = (int)packetLength - paddingLength - 1;

				// check packet size
				if (totalPacketLength < MIN_PACKET_SIZE || totalPacketLength > MAX_PACKET_SIZE)
				{
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, $"Packet size must be between {MIN_PACKET_SIZE} and {MAX_PACKET_SIZE} bytes (inclusive)", null);
					return;
				}

				// check padding
				if (paddingLength < MIN_PADDING_BYTES)
				{
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, $"Padding must be of at least {MIN_PADDING_BYTES} bytes", null);
					return;
				}

				// encrypted data length must be a multiple of the block size
				if (totalEncryptedLength % blockSize != 0)
				{
					remotePeer.Disconnect(DisconnectionReason.ProtocolError, $"Encrypted data size muse be a multiple of the block size", null);
					return;
				}

				// read the rest of the packet into `buffer`
				read += await remotePeer.Stream.ReadAtLeast(rawPacketBuffer.Array, read, totalPacketLength - read, totalPacketLength - read, cancellationToken);
				if (read < totalPacketLength)
				{
					remotePeer.Disconnect(DisconnectionReason.ConnectionLost);
					return;
				}

				// decrypt the rest of the packet (`packetLength` - the already decrypted first block's `blockSize`)
				encryption.Decrypt(rawPacketBuffer.Array, blockSize, totalEncryptedLength - blockSize, decryptedBuffer.Array, decrypted);
				decrypted += totalEncryptedLength - blockSize;

				// if we have a MAC, we verify it
				if (mac.DigestLength > 0)
				{
					using (var hashBuffer = ByteArrayPool.Acquire(mac.DigestLength))
					{
						mac.ComputeHash(messageSequenceNumber, decryptedBuffer, 0, decrypted, hashBuffer, 0);

						// compare remote MAC with the locally-computed one
						if (!rawPacketBuffer.Array.RangeEquals(totalPacketLength - mac.DigestLength, hashBuffer, 0, mac.DigestLength))
						{
							remotePeer.Disconnect(DisconnectionReason.MACError, "Computed MAC does not match", null);
							return;
						}
					}
				}

				// decompress the payload
				int decompressed;
				try
				{
					decompressed = compression.Decompress(decryptedBuffer.Array, 4 + 1, totalCompressedLength, decompressedBuffer.Array, 0);
				}
				catch (Exception e)
				{
					RaiseException(remotePeer, e);

					remotePeer.Disconnect(DisconnectionReason.CompressionError);
					return;
				}
				finally
				{
					// zero-out the decrypted array before returning it to the pool
					Array.Clear(decryptedBuffer.Array, 0, decrypted);
				}

				try
				{
					// process payload
					await ProcessMessage(remotePeer, decompressedBuffer.Array, 0, decompressed, messageSequenceNumber, cancellationToken);
				}
				finally
				{
					// zero-out the decompressed payload array before returning it to the pool
					Array.Clear(decompressedBuffer.Array, 0, decompressed);
				}
			}
		}

		/// <summary>Do not call this function directly, use <see cref="RemotePeer.SendMessage(byte[], int, int)"/>.</summary>
		/// <param name="remotePeer"></param>
		/// <param name="message"></param>
		/// <param name="messageOffset"></param>
		/// <param name="messageLength"></param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		async Task sendPacket(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, CancellationToken cancellationToken)
		{
			// https://github.com/TyrenDe/SSHServer/wiki/010:-Sending-A-Packet

			// PACKET FORMAT:
			// <encrypted>
			//   <mac>
			//     uint32 packet_length
			//     <packet_length>
			//       byte padding_length
			//       <compressed>
			//         byte[n1] payload; n1 = packet_length - padding_length - 1
			//       </compressed>
			//       byte[n2] random padding; n2 = padding_length
			//     </packet_length>
			//   </mac>
			// </encrypted>
			// byte[m] mac(Message Authentication Code -MAC); m = mac_length

			if (messageLength > MAX_UNCOMPRESSED_PAYLOAD_SIZE)
				throw new ArgumentException("Messages cannot exceed " + MAX_UNCOMPRESSED_PAYLOAD_SIZE + " bytes in length", nameof(messageLength));

			using (var unencryptedBuffer = ByteArrayPool.Acquire(MAX_PACKET_SIZE))
			using (var encryptedBuffer = ByteArrayPool.Acquire(MAX_PACKET_SIZE))
			{
				uint messageSequenceNumber;
				IEncryptionAlgorithm encryption;
				IMACAlgorithm mac;
				ICompressionAlgorithm compression;

				// NOTE: `ExchangeContext` must always be used with a `lock` over itself.
				lock (remotePeer.ExchangeContext)
				{
					// read and increment message sequence number
					messageSequenceNumber = unchecked(remotePeer.ExchangeContext.NextLocalToRemoteMessageSequenceNumber++);

					encryption = remotePeer.ExchangeContext.LocalToRemoteEncryptionAlgorithm;
					mac = remotePeer.ExchangeContext.LocalToRemoteMACAlgorithm;
					compression = remotePeer.ExchangeContext.LocalToRemoteCompressionAlgorithm;
				}

				// raise "sending message" event
				RaiseSendingMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber);

				// compress the payload
				var totalCompressedLength = compression.Compress(message, messageOffset, messageLength, unencryptedBuffer.Array, 4 + 1);

				// compute sizes
				var blockSize = (int)Math.Ceiling(8d / encryption.BlockSize) * encryption.BlockSize;
				var unpaddedEncryptedLength = 4 + 1 + totalCompressedLength;
				var paddingLength = blockSize - (unpaddedEncryptedLength % blockSize);
				if (paddingLength < MIN_PADDING_BYTES)
					paddingLength += blockSize;

				var totalEncryptedLength = unpaddedEncryptedLength + paddingLength;
				var totalPacketLength = totalEncryptedLength + mac.DigestLength;
				var packetLength = totalEncryptedLength - 4;

				// write packet length and padding length
				BitConverterBigEndian.FromUInt32((uint)packetLength, unencryptedBuffer.Array, 0);
				unencryptedBuffer.Array[4] = (byte)paddingLength;

				// generate padding bytes
				remotePeer.FillRandomBytes(unencryptedBuffer.Array, unpaddedEncryptedLength, paddingLength);

				// encrypt
				encryption.Encrypt(unencryptedBuffer.Array, 0, totalEncryptedLength, encryptedBuffer.Array, 0);

				// compute MAC
				mac.ComputeHash(messageSequenceNumber, unencryptedBuffer.Array, 0, totalEncryptedLength, encryptedBuffer.Array, totalEncryptedLength);

				// zero-out the unencrypted payload array before returning it to the pool
				Array.Clear(unencryptedBuffer.Array, 0, totalEncryptedLength);

				await remotePeer.Stream.WriteAsync(encryptedBuffer.Array, 0, totalPacketLength);
			}
		}

		/// <summary>Process a received packet payload.</summary>
		/// <param name="message">Message data buffer.</param>
		/// <param name="messageOffset">Where to start reading the message at.</param>
		/// <param name="messageLength">How long is the message.</param>
		/// <param name="cancellationToken">Cancellation token used to interrupt the operation.</param>
		/// <returns>Task for the operation.</returns>
		/// <remarks>Do not keep a reference to payload data buffer, the array will be recycled by other packets.</remarks>
		protected virtual Task ProcessMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			// raise "message received" event
			RaiseMessageReceived(remotePeer, message, messageOffset, messageLength, messageSequenceNumber);

			var messageType = (MessageType)message[0];

			switch (messageType)
			{
				case MessageType.Disconnect:
					return ProcessDisconnectMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.Ignore:
					return ProcessIgnoreMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.Unimplemented:
					return ProcessUnimplementedMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.Debug:
					return ProcessDebugMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.ServiceRequest:
					// TODO
					remotePeer.Disconnect(DisconnectionReason.ByApplication, "TODO: not implemented", null);
					return Task.CompletedTask;

				case MessageType.ServiceAccept:
					// TODO
					remotePeer.Disconnect(DisconnectionReason.ByApplication, "TODO: not implemented", null);
					return Task.CompletedTask;

				case MessageType.KeyExchangeInitialization:
					return ProcessKeyExchangeInitializationMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.NewKeys:
					return ProcessNewKeysMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.DiffieHellmanKeyExchangeInitialization:
					return ProcessDiffieHellmanKeyExchangeInitializationMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				case MessageType.DiffieHellmanKeyExchangeReply:
					return ProcessDiffieHellmanKeyExchangeReplyMessage(remotePeer, message, messageOffset, messageLength, messageSequenceNumber, cancellationToken);

				default:
					return remotePeer.SendUnimplementedMessage(messageSequenceNumber);
			}
		}

		protected virtual Task ProcessIgnoreMessage(RemotePeer client, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			// nothing to do
			return Task.CompletedTask;
		}

		protected virtual Task ProcessUnimplementedMessage(RemotePeer client, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			// we don't handle this
			return Task.CompletedTask;
		}

		protected virtual Task ProcessDisconnectMessage(RemotePeer client, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			// we must not send any reply when receiving this message, we must disconnect client immediately
			client.DisconnectImmediate();

			return Task.CompletedTask;
		}

		protected virtual Task ProcessDebugMessage(RemotePeer remotePeer, byte[] message, int messageOffset, int messageLength, uint messageSequenceNumber, CancellationToken cancellationToken)
		{
			DebugMessage.Read(message, messageOffset, messageLength, out bool alwaysDisplay, out string debugMessage, out string language);

			// raise "debug message received" event
			RaiseDebugMessageReceived(remotePeer, alwaysDisplay, debugMessage, language);

			return Task.CompletedTask;
		}
	}
}
