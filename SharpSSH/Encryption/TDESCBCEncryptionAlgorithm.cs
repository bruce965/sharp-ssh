﻿using SharpSSH.Util;
using System.IO;
using System.Security.Cryptography;

namespace SharpSSH.Encryption
{
	public class TDESCBCEncryptionAlgorithm : IEncryptionAlgorithm
	{
		public static readonly string AlgorithmName = "3des-cbc";

		public static readonly IAlgorithmFactory<TDESCBCEncryptionAlgorithm> Factory = AlgorithmFactory.For<TDESCBCEncryptionAlgorithm>(AlgorithmName);

		readonly TripleDESCryptoServiceProvider crypto = new TripleDESCryptoServiceProvider() { KeySize = 192, Padding = PaddingMode.None, Mode = CipherMode.CBC };
		ICryptoTransform encryptor;
		ICryptoTransform decryptor;

		public int BlockSize => crypto.BlockSize / 8;

		public int KeySize => crypto.KeySize / 8;

		public void Encrypt(byte[] data, int dataOffset, int dataCount, byte[] encryptedData, int encryptedDataOffset)
		{
			lock (crypto)
			{
				encryptor.TransformBlock(data, dataOffset, dataCount, encryptedData, encryptedDataOffset);
			}
		}

		public void Decrypt(byte[] data, int dataOffset, int dataCount, byte[] decryptedData, int decryptedDataOffset)
		{
			lock (crypto)
			{
				decryptor.TransformBlock(data, dataOffset, dataCount, decryptedData, decryptedDataOffset);
			}
		}

		public void SetKey(byte[] key, int keyOffset, int keyLength, byte[] iv, int ivOffset, int ivLength)
		{
			lock (crypto)
			{
				if (encryptor != null)
					encryptor.Dispose();

				if (decryptor != null)
					decryptor.Dispose();

				var cryptoKey = key.Slice(keyOffset, keyLength);
				var cryptoIV = iv.Slice(ivOffset, ivLength);

				encryptor = crypto.CreateEncryptor(cryptoKey, cryptoIV);
				decryptor = crypto.CreateDecryptor(cryptoKey, cryptoIV);
			}
		}
	}
}
