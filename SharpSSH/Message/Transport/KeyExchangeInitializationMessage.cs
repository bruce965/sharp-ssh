﻿using SharpSSH.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class KeyExchangeInitializationMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-7.1

		public static Task SendKeyExchangeInitializationMessage(
			this IRemotePeer client,
			byte[] cookie,
			IEnumerable<string> kexAlgorithms,
			IEnumerable<string> serverHostKeyAlgorithms,
			IEnumerable<string> encryptionAlgorithmsClientToServer, IEnumerable<string> encryptionAlgorithmsServerToClient,
			IEnumerable<string> macAlgorithmsClientToServer, IEnumerable<string> macAlgorithmsServerToClient,
			IEnumerable<string> compressionAlgorithmsClientToServer, IEnumerable<string> compressionAlgorithmsServerToClient,
			IEnumerable<string> languagesClientToServer, IEnumerable<string> languagesServerToClient,
			bool firstKexPacketFollows
		)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(
					buffer, 0,
					cookie,
					kexAlgorithms,
					serverHostKeyAlgorithms,
					encryptionAlgorithmsClientToServer, encryptionAlgorithmsServerToClient,
					macAlgorithmsClientToServer, macAlgorithmsServerToClient,
					compressionAlgorithmsClientToServer, compressionAlgorithmsServerToClient,
					languagesClientToServer, languagesServerToClient,
					firstKexPacketFollows
				);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(
			byte[] buffer, int offset, int length,
			out byte[] cookie,
			out string[] kexAlgorithms,
			out string[] serverHostKeyAlgorithms,
			out string[] encryptionAlgorithmsClientToServer, out string[] encryptionAlgorithmsServerToClient,
			out string[] macAlgorithmsClientToServer, out string[] macAlgorithmsServerToClient,
			out string[] compressionAlgorithmsClientToServer, out string[] compressionAlgorithmsServerToClient,
			out string[] languagesClientToServer, out string[] languagesServerToClient,
			out bool firstKexPacketFollows
		)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.KeyExchangeInitialization)
				throw new FormatException($"Expecting message type: {MessageType.KeyExchangeInitialization.GetName()}");

			cookie = new byte[16];
			reader.Read(cookie, 0, 16);
			kexAlgorithms = reader.ReadNameList();
			serverHostKeyAlgorithms = reader.ReadNameList();
			encryptionAlgorithmsClientToServer = reader.ReadNameList();
			encryptionAlgorithmsServerToClient = reader.ReadNameList();
			macAlgorithmsClientToServer = reader.ReadNameList();
			macAlgorithmsServerToClient = reader.ReadNameList();
			compressionAlgorithmsClientToServer = reader.ReadNameList();
			compressionAlgorithmsServerToClient = reader.ReadNameList();
			languagesClientToServer = reader.ReadNameList();
			languagesServerToClient = reader.ReadNameList();
			firstKexPacketFollows = reader.ReadBoolean();
			var reserved = reader.ReadUInt32();

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");

			if (reserved != 0)
				throw new FormatException("Expected a trailing 0 (reserved for future extension)");
		}

		public static int Write(
			byte[] buffer, int offset,
			byte[] cookie,
			IEnumerable<string> kexAlgorithms,
			IEnumerable<string> serverHostKeyAlgorithms,
			IEnumerable<string> encryptionAlgorithmsClientToServer, IEnumerable<string> encryptionAlgorithmsServerToClient,
			IEnumerable<string> macAlgorithmsClientToServer, IEnumerable<string> macAlgorithmsServerToClient,
			IEnumerable<string> compressionAlgorithmsClientToServer, IEnumerable<string> compressionAlgorithmsServerToClient,
			IEnumerable<string> languagesClientToServer, IEnumerable<string> languagesServerToClient,
			bool firstKexPacketFollows
		)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.KeyExchangeInitialization);
			writer.Write(cookie, 0, 16);
			writer.WriteNameList(kexAlgorithms);
			writer.WriteNameList(serverHostKeyAlgorithms);
			writer.WriteNameList(encryptionAlgorithmsClientToServer);
			writer.WriteNameList(encryptionAlgorithmsServerToClient);
			writer.WriteNameList(macAlgorithmsClientToServer);
			writer.WriteNameList(macAlgorithmsServerToClient);
			writer.WriteNameList(compressionAlgorithmsClientToServer);
			writer.WriteNameList(compressionAlgorithmsServerToClient);
			writer.WriteNameList(languagesClientToServer);
			writer.WriteNameList(languagesServerToClient);
			writer.WriteBoolean(firstKexPacketFollows);
			writer.WriteUInt32(0);  // reserved for future extension

			return writer.MessageLength;
		}
	}
}
