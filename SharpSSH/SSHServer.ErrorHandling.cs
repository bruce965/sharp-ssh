﻿using System;
using System.Threading.Tasks;

namespace SharpSSH
{
	partial class SSHServer
	{
		#region Events

		/// <summary>An exception was thrown.</summary>
		public event SSHServerEvent<ExceptionEventArgs> Exception;

		/// <summary>Raise the <see cref="Exception"/> event to report an exception.</summary>
		/// <param name="remotePeer">The remote peer that caused the exception, if any.</param>
		/// <param name="e">Exception to be reported.</param>
		protected virtual void RaiseException(IRemotePeer remotePeer, Exception e)
		{
			if (Exception == null)
				return;

			raiseSafely(Exception, new ExceptionEventArgs(remotePeer, e));
		}

		#endregion

		/// <summary>Raise an event and process exceptions from handlers.</summary>
		/// <typeparam name="T">Arguments of the event handler delegate.</typeparam>
		/// <param name="evt">Event to be triggered.</param>
		/// <param name="argsFactory">Function that returns arguments.</param>
		void raiseSafely<T>(SSHServerEvent<T> evt, Func<T> argsFactory) where T : SSHServerEventArgs
		{
			foreach (SSHServerEvent<T> handler in evt.GetInvocationList())
			{
				var args = argsFactory();

				try
				{
					// NOTE: each individual handler is executed in it's own `try` block
					handler(this, args);
				}
				catch (Exception e)
				{
					// avoid infinite recursion if the exception handler throws an exception
					if (ReferenceEquals(evt, Exception))
						continue;

					// if the handler throws, report the exception
					RaiseException(args.RemotePeer, e);
				}
			}
		}

		/// <summary>Raise an event and process exceptions from handlers.</summary>
		/// <typeparam name="T">Arguments of the event handler delegate.</typeparam>
		/// <param name="evt">Event to be triggered.</param>
		/// <param name="args">Arguments.</param>
		void raiseSafely<T>(SSHServerEvent<T> evt, T args) where T : SSHServerEventArgs => raiseSafely(evt, () => args);

		/// <summary>Report exceptions from a task.</summary>
		/// <param name="task">Task to report exceptions from.</param>
		void handleExceptions(IRemotePeer remotePeer, Task task)
		{
			// if the task throws, report the exception
			task.ContinueWith(taskRef => RaiseException(remotePeer, taskRef.Exception), TaskContinuationOptions.OnlyOnFaulted);
		}
	}
}
