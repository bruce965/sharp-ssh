﻿using SharpSSH.Util;
using System;
using System.Text;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class IgnoredDataMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-11.2

		public static Task SendIgnoredDataMessage(this IRemotePeer client, string data)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0, data);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out string data)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.Ignore)
				throw new FormatException($"Expecting message type: {MessageType.Ignore.GetName()}");

			data = reader.ReadString(Encoding.UTF8);

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset, string data)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.Ignore);
			writer.WriteString(data, Encoding.UTF8);

			return writer.MessageLength;
		}
	}
}
