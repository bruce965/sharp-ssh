﻿using System.Net;
using System.Threading.Tasks;

namespace SharpSSH
{
	/// <summary>Remote SSH client or server.</summary>
	public interface IRemotePeer
	{
		/// <summary>Is this peer an SSH server? If not, it is an SSH client.</summary>
		bool IsServer { get; }

		/// <summary>Remote endpoint of this client.</summary>
		EndPoint RemoteEndPoint { get; }

		/// <summary>Protocol version of this client, if known.</summary>
		ProtocolVersion ProtocolVersion { get; }

		/// <summary>Disconnection reason, if the client is disconnecting/disconnected.</summary>
		DisconnectionReason DisconnectReason { get; }

		/// <summary>Language of this client, all messages sent to the client should be localized in this language.</summary>
		string Language { get; }

		/// <summary>Description of the disconnection reason, if the client is disconnecting/disconnected.</summary>
		string DisconnectDescription { get; }

		/// <summary>Language of the disconnection reason description.</summary>
		string DisconnectDescriptionLanguage { get; }

		/// <summary>Send a message to this client.</summary>
		/// <param name="data">Content of the message.</param>
		/// <param name="offset">Offset of the message inside <paramref name="data"/>.</param>
		/// <param name="length">Length of the message in bytes.</param>
		/// <returns>Task resolved after message is sent.</returns>
		Task SendMessage(byte[] data, int offset, int length);

		/// <summary>Disconnect this client.</summary>
		/// <param name="disconnectReason">Disconnection reason.</param>
		/// <param name="description">Disconnection reason description.</param>
		/// <param name="language">Language of the disconnection reason description.</param>
		void Disconnect(DisconnectionReason disconnectReason, string description, string language);

		/// <summary>Disconnect this client.</summary>
		/// <param name="disconnectReason">Disconnection reason.</param>
		void Disconnect(DisconnectionReason disconnectReason);
	}
}
