﻿using System;
using System.Text;

namespace SharpSSH
{
	// https://tools.ietf.org/html/rfc4253#section-11.1
	public enum DisconnectionReason : UInt32
	{
		/// <summary>Disconnect without sending a disconnection message.</summary>
		None = 0,
		/// <summary>SSH_DISCONNECT_HOST_NOT_ALLOWED_TO_CONNECT</summary>
		HostNotAllowedToConnect = 1,
		/// <summary>SSH_DISCONNECT_PROTOCOL_ERROR</summary>
		ProtocolError = 2,
		/// <summary>SSH_DISCONNECT_KEY_EXCHANGE_FAILED</summary>
		KeyExchangeFailed = 3,
		/// <summary>SSH_DISCONNECT_RESERVED</summary>
		Reserved = 4,
		/// <summary>SSH_DISCONNECT_MAC_ERROR</summary>
		MACError = 5,
		/// <summary>SSH_DISCONNECT_COMPRESSION_ERROR</summary>
		CompressionError = 6,
		/// <summary>SSH_DISCONNECT_SERVICE_NOT_AVAILABLE</summary>
		ServiceNotAvailable = 7,
		/// <summary>SSH_DISCONNECT_PROTOCOL_VERSION_NOT_SUPPORTED</summary>
		ProtocolVersionNotSupported = 8,
		/// <summary>SSH_DISCONNECT_HOST_KEY_NOT_VERIFIABLE</summary>
		HostKeyNotVerifiable = 9,
		/// <summary>SSH_DISCONNECT_CONNECTION_LOST</summary>
		ConnectionLost = 10,
		/// <summary>SSH_DISCONNECT_BY_APPLICATION</summary>
		ByApplication = 11,
		/// <summary>SSH_DISCONNECT_TOO_MANY_CONNECTIONS</summary>
		TooManyConnections = 12,
		/// <summary>SSH_DISCONNECT_AUTH_CANCELLED_BY_USER</summary>
		AuthenticationCancelledByUser = 13,
		/// <summary>SSH_DISCONNECT_NO_MORE_AUTH_METHODS_AVAILABLE</summary>
		NoMoreAuthenticationMethodsAvailable = 14,
		/// <summary>SSH_DISCONNECT_ILLEGAL_USER_NAME</summary>
		IllegalUserName = 15
	}

	public static class DisconnectReasonExtensions
	{
		/// <summary>Get a string representation of this disconnection reason.</summary>
		/// <param name="type">Disconnection reason.</param>
		/// <returns>Official representation of the disconnection reason, or a fallback representation for custom reasons.</returns>
		public static string GetName(this DisconnectionReason type)
		{
			var sb = new StringBuilder(64);
			sb.Append("SSH_DISCONNECT_");

			switch (type)
			{
				case DisconnectionReason.HostNotAllowedToConnect:
					sb.Append("HOST_NOT_ALLOWED_TO_CONNECT");
					break;
				case DisconnectionReason.ProtocolError:
					sb.Append("PROTOCOL_ERROR");
					break;
				case DisconnectionReason.KeyExchangeFailed:
					sb.Append("KEY_EXCHANGE_FAILED");
					break;
				case DisconnectionReason.Reserved:
					sb.Append("RESERVED");
					break;
				case DisconnectionReason.MACError:
					sb.Append("MAC_ERROR");
					break;
				case DisconnectionReason.CompressionError:
					sb.Append("COMPRESSION_ERROR");
					break;
				case DisconnectionReason.ServiceNotAvailable:
					sb.Append("SERVICE_NOT_AVAILABLE");
					break;
				case DisconnectionReason.ProtocolVersionNotSupported:
					sb.Append("PROTOCOL_VERSION_NOT_SUPPORTED");
					break;
				case DisconnectionReason.HostKeyNotVerifiable:
					sb.Append("HOST_KEY_NOT_VERIFIABLE");
					break;
				case DisconnectionReason.ConnectionLost:
					sb.Append("CONNECTION_LOST");
					break;
				case DisconnectionReason.ByApplication:
					sb.Append("BY_APPLICATION");
					break;
				case DisconnectionReason.TooManyConnections:
					sb.Append("TOO_MANY_CONNECTIONS");
					break;
				case DisconnectionReason.AuthenticationCancelledByUser:
					sb.Append("AUTH_CANCELLED_BY_USER");
					break;
				case DisconnectionReason.NoMoreAuthenticationMethodsAvailable:
					sb.Append("NO_MORE_AUTH_METHODS_AVAILABLE");
					break;
				case DisconnectionReason.IllegalUserName:
					sb.Append("ILLEGAL_USER_NAME");
					break;
				default:
					sb.Append("CUSTOM_");
					sb.Append((int)type);
					break;
			}

			return sb.ToString();
		}
	}
}
