﻿using System;
using System.IO;

namespace SharpSSH
{
	// According to 2015 Microsoft .NET Docs, using type `object` for the `sender` is not required anymore:
	//
	// «Generic delegates are especially useful in defining events based on the typical design pattern because
	// the sender argument can be strongly typed and no longer has to be cast to and from Object.»
	// — https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/generic-delegates
	//
	// Although, the 2017 "event handler design guidelines" do state the opposite:
	//
	// «DO use object as the type of the first parameter of the event handler, and call it sender.»
	// — https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/event#custom-event-handler-design
	//
	// Honestly, I prefer strongly-typing `sender` and will stick to this convention unless I'm given
	// a good reason to avoid this practice.

	public delegate void SSHServerEvent<TEventArgs>(SSHServer sender, TEventArgs e) where TEventArgs : SSHServerEventArgs;

	public class SSHServerEventArgs : EventArgs
	{
		public static new readonly SSHServerEventArgs Empty = new SSHServerEventArgs(null);

		/// <summary>The remote peer that caused this event, if any.</summary>
		public IRemotePeer RemotePeer { get; }

		public SSHServerEventArgs(IRemotePeer remotePeer) => RemotePeer = remotePeer;
	}

	public class ExceptionEventArgs : SSHServerEventArgs
	{
		public Exception Exception { get; }

		public ExceptionEventArgs(IRemotePeer remotePeer, Exception exception) : base(remotePeer) => Exception = exception;
	}

	public class MessageEventArgs : SSHServerEventArgs
	{
		/// <summary>Readable uncompresed decrypted payload of the SSH message.</summary>
		public Stream Payload { get; }

		/// <summary>Session-unique message sequence number.</summary>
		public uint MessageSequenceNumber { get; }

		public MessageEventArgs(IRemotePeer remotePeer, byte[] data, int offset, int length, uint messageSequenceNumber) : base(remotePeer)
		{
			Payload = new MemoryStream(data, offset, length, false, false);
			MessageSequenceNumber = messageSequenceNumber;
		}
	}

	public class DebugMessageEventArgs : SSHServerEventArgs
	{
		/// <summary>Should this message always be displayed regardless of debug settings?</summary>
		public bool AlwaysDisplay { get; }

		/// <summary>Content of the message.</summary>
		public string Message { get; }

		/// <summary>Language of the message.</summary>
		public string Language{ get; }

		public DebugMessageEventArgs(IRemotePeer remotePeer, bool alwaysDisplay, string message, string language) : base(remotePeer)
		{
			AlwaysDisplay = alwaysDisplay;
			Message = message;
			Language = language;
		}
	}
}
