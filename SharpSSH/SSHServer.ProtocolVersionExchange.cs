﻿using SharpSSH.Util;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH
{
	partial class SSHServer
	{
		partial class RemotePeer
		{
			readonly object protocolVersionLock = new object();
			ProtocolVersion protocolVersion;

			public ProtocolVersion ProtocolVersion
			{
				get { lock (protocolVersionLock) return protocolVersion; }
				set { lock (protocolVersionLock) protocolVersion = value; }
			}
		}

		/// <summary>How many bytes to read from stream before aborting while reading protocol version.</summary>
		const int PROTOCOL_VERSION_EXCHANGE_MAX_BYTES = 64 * 1024;  // 64 KB

		string softwareVersion = "SharpSSH";
		/// <summary>An arbitrary string identifying this SSH software.</summary>
		public string SoftwareVersion
		{
			get => softwareVersion;
			set => softwareVersion = (value ?? "").Replace('-', '_').Replace(' ', '_');
		}

		string comments = "";
		/// <summary>Optional string sent along during protocol version exchange.</summary>
		public string Comments
		{
			get => comments;
			set => comments = (value ?? "").Replace("\r\n", "\n").Replace('\n', ' ');
		}

		/// <summary>
		/// Protocol version of the server.
		/// 
		/// This property cannot be set directly, use <see cref="SoftwareVersion"/> and <see cref="Comments"/> to set the value of this property.
		/// </summary>
		public ProtocolVersion ProtocolVersion => new ProtocolVersion(PROTO_VERSION, SoftwareVersion, Comments);

		// https://tools.ietf.org/html/rfc4253#section-4.2
		async Task<ProtocolVersion?> readProtocolVersionExchange(RemotePeer remotePeer, CancellationToken cancellationToken)
		{
			using (var buffer = ByteArrayPool.Acquire(PROTOCOL_VERSION_STRING_MAX_BYTES))
			{
				var totalRead = 0;
				var linePosition = 0;

				while (linePosition < PROTOCOL_VERSION_STRING_MAX_BYTES && totalRead < PROTOCOL_VERSION_EXCHANGE_MAX_BYTES && !cancellationToken.IsCancellationRequested)
				{
					var read = await remotePeer.Stream.ReadAsync(buffer, linePosition, 1, cancellationToken);
					if (read == 0)
					{
						remotePeer.Disconnect(DisconnectionReason.ConnectionLost);
						return null;
					}

					linePosition += read;
					totalRead += read;

					// keep reading until the first CR LF sequence
					if (linePosition >= 2 && buffer.Array[linePosition - 2] == '\r' && buffer.Array[linePosition - 1] == '\n')
					{
						// if line does not start with UTF-8 "SSH-" string, dischard the whole line
						if (linePosition < 6 || buffer.Array[0] != 'S' || buffer.Array[1] != 'S' || buffer.Array[2] != 'H' || buffer.Array[3] != '-')
						{
							linePosition = 0;
							continue;
						}

						// else remove the trailing CR LF
						var str = Encoding.UTF8.GetString(buffer, 0, linePosition - 2);

						// parse the protocol version
						var protocolVersion = new ProtocolVersion(str);
						if (!protocolVersion.IsValid)
						{
							remotePeer.Disconnect(DisconnectionReason.ProtocolError, "Invalid identifier", null);
							return null;
						}

						return protocolVersion;
					}

					// if the buffer is full, we empty it and keep the last character because it might be the CR of a CR-LF sequence
					// we prefix it with a random character to avoid accidentally picking up an "SSH-" sequence in the middle of the line
					if (linePosition == PROTOCOL_VERSION_STRING_MAX_BYTES)
					{
						buffer.Array[0] = (byte)'X';  // random character, anything except the 'S' in the "SSH-" sequence is fine
						buffer.Array[1] = buffer.Array[linePosition - 1];  // the last read character, that might be a CR
						linePosition = 2;
					}
				}

				remotePeer.Disconnect(DisconnectionReason.ConnectionLost);
				return null;
			}
		}

		Task sendProtocolVersionExchange(Stream stream, CancellationToken cancellationToken)
		{
			var protocolVersion = ProtocolVersion;
			if (!protocolVersion.IsValid)
				throw new InvalidDataException("Invalid server protocol version: " + protocolVersion);

			using (var buffer = ByteArrayPool.Acquire(PROTOCOL_VERSION_STRING_MAX_BYTES))
			{
				// build "SSH-protoversion-softwareversion SP comments" bytes
				var index = Encoding.UTF8.GetBytes(protocolVersion.IdentificationString, 0, protocolVersion.IdentificationString.Length, buffer, 0);

				// add trailing CR LF
				buffer.Array[index++] = (byte)'\r';
				buffer.Array[index++] = (byte)'\n';

				// send
				return stream.WriteAsync(buffer, 0, index);
			}
		}
	}
}
