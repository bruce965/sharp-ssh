﻿using SharpSSH.Util;
using System;
using System.Numerics;
using System.Threading.Tasks;

using static SharpSSH.SSHConstants;

namespace SharpSSH.Message
{
	public static class DiffieHellmanKeyExchangeReplyMessage
	{
		// https://tools.ietf.org/html/rfc4253#section-8

		public static Task SendDiffieHellmanKeyExchangeReplyMessage(this IRemotePeer client, byte[] K_S, BigInteger f, byte[] signatureOfH)
		{
			using (var buffer = ByteArrayPool.Acquire(MAX_UNCOMPRESSED_PAYLOAD_SIZE))
			{
				var messageLength = Write(buffer, 0, K_S, f, signatureOfH);
				return client.SendMessage(buffer, 0, messageLength);
			}
		}

		public static void Read(byte[] buffer, int offset, int length, out byte[] K_S, out BigInteger f, out byte[] signatureOfH)
		{
			var reader = new SSHMessageReader(buffer, offset);
			var messageType = (MessageType)reader.ReadByte();

			if (messageType != MessageType.DiffieHellmanKeyExchangeReply)
				throw new FormatException($"Expecting message type: {MessageType.DiffieHellmanKeyExchangeReply.GetName()}");

			K_S = reader.ReadBytes();
			f = reader.ReadMPInt();
			signatureOfH = reader.ReadBytes();

			if (reader.MessageLength != length)
				throw new FormatException("Invalid message length");
		}

		public static int Write(byte[] buffer, int offset, byte[] K_S, BigInteger f, byte[] signatureOfH)
		{
			var writer = new SSHMessageWriter(buffer, offset);
			writer.WriteByte((byte)MessageType.DiffieHellmanKeyExchangeReply);
			writer.WriteBytes(K_S);
			writer.WriteMPInt(f);
			writer.WriteBytes(signatureOfH);

			return writer.MessageLength;
		}
	}
}
