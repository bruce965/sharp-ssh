﻿using SharpSSH.Util;
using System.IO;
using System.Security.Cryptography;

namespace SharpSSH.MAC
{
	public class HMACSHA1MACAlgorithm : IMACAlgorithm
	{
		// https://tools.ietf.org/html/rfc4253#section-6.4

		public static readonly string AlgorithmName = "hmac-sha1";

		public static readonly IAlgorithmFactory<HMACSHA1MACAlgorithm> Factory = AlgorithmFactory.For<HMACSHA1MACAlgorithm>(AlgorithmName);

		public int KeySize => 20;

		public int DigestLength => 20;

		readonly HMACSHA1 sha1 = new HMACSHA1();

		public void SetKey(byte[] key, int offset, int length)
		{
			lock (sha1)
			{
				sha1.Key = key.Slice(offset, length);
			}
		}

		public void ComputeHash(uint packetNumber, byte[] data, int dataOffset, int dataCount, byte[] hash, int hashOffset)
		{
			lock (sha1)
			{
				using (var buffer = ByteArrayPool.Acquire(4)) // size of UInt32
				{
					var writer = new SSHMessageWriter(buffer, 0);
					writer.WriteUInt32(packetNumber);

					using (var packetNumberStream = new MemoryStream(buffer.Array, 0, writer.MessageLength, false, false))
					using (var dataStream = new MemoryStream(data, dataOffset, dataCount, false, false))
					{
						var fullStream = packetNumberStream.Concat(dataStream);

						sha1.ComputeHash(fullStream).CopyTo(0, hash, hashOffset, DigestLength);
					}
				}
			}
		}
	}
}
